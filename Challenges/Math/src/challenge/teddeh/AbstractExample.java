package challenge.teddeh;

public abstract class AbstractExample implements Game {

    String name;

    public AbstractExample(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
