package challenge.teddeh;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Main
{
	public static void main(String[] args)
	{
		Game game = new Example();
//		Game test = example.getClass().getAnnotation(Game.class);
//		System.out.println(example.getName());

		System.out.println(game.getName());
	}

//	//Factorial Recursion   5! = 120 (5 * 4 * 3 * 2 * 1)
//	private static int getFactorial(int number)
//	{
//		if(number < 2) return 1;
//		return getFactorial(number-1) * number;
//	}
//
//	/**
//	 * @Challenger TOXIC
//	 *
//	 * @Description
//	 * Get the remainder without using a modulo ( x % z )
//	 */
//	private static int getRemainder(int num1, int num2)
//	{
//		return num1 - (num1 / num2) * num2;
//	}
//
//	/**
//	 * @Challenger Adam
//	 */
//	private static int fibonacci(int amount)
//	{
//		if(amount < 2) return 1;
//		return fibonacci(amount - 1) + fibonacci(amount - 2);
//	}
//
//	private static List<Integer> getDoors()
//	{
//		List<Integer> doors = new ArrayList<>();
//		for(int monk = 1; monk < 65; monk++) {
//			for(int door = 1; door < 65; door++) {
//				if(door % monk == 0) {
//					if(doors.contains(door)) doors.remove((Object)door);
//					else doors.add(door);
//				}
//			}
//		}
//		return doors;
//	}
}
