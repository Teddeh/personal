package challenge.teddeh;

import java.util.Random;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Main
{
	/**
	 * @Challenger Code Homework
	 *
	 * @Description
	 * Write a program that creates and prints a random phone number of the form XXX-XXX-XXXX.
	 * Include the dashes in the output.
	 * Do not let the first three digits contain an 8 or 9 (but don't be more restrictive than that),
	 * and make sure that the second set of three digits is not greater than 742.
	 */

	public static void main(String[] args)
	{
		Random random = new Random();
		for(int i = 1; i < 11; i++)
		{
			System.out.println("#" + i + ": " + generateNumber(random));
		}
	}

	private static String generateNumber(Random random)
	{
		int[][] numbers = new int[][]
				{
						{
								random.nextInt(8),
								random.nextInt(8),
								random.nextInt(8)
						},
						{
								random.nextInt(8),
								random.nextInt(5),
								random.nextInt(3)
						},
						{
								random.nextInt(10),
								random.nextInt(10),
								random.nextInt(10),
								random.nextInt(10)
						}
				};

		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < numbers.length; i++)
		{
			for(int x = 0; x < numbers[i].length; x++)
			{
				builder.append(numbers[i][x]);
			}

			if(i != (numbers.length-1)) builder.append("-");
		}

		return builder.toString();
	}
}
