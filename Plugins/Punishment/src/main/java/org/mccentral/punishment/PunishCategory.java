package org.mccentral.punishment;

import org.mccentral.punishment.severity.Severity;
import org.mccentral.punishment.severity.SubSeverity;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum PunishCategory
{
	CHAT_OFFENCE(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (15*60)*1000, SubSeverity.Type.NULL),//15 mins
					new SubSeverity(2, (30*60)*1000, SubSeverity.Type.NULL),//30 mins
					new SubSeverity(3, (45*60)*1000, SubSeverity.Type.NULL),//45 mins
					new SubSeverity(4, (60*60)*1000, SubSeverity.Type.NULL),//60 mins
					new SubSeverity(5, (75*60)*1000, SubSeverity.Type.NULL)// 75 mins
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, (((2*24)*60)*60)*1000, SubSeverity.Type.NULL),//2 days
					new SubSeverity(2, (((4*24)*60)*60)*1000, SubSeverity.Type.NULL),//4 days
					new SubSeverity(3, (((5*24)*60)*60)*1000, SubSeverity.Type.NULL),//5 days
					new SubSeverity(4, (((7*24)*60)*60)*1000, SubSeverity.Type.NULL),//7 days
					new SubSeverity(5, (((14*24)*60)*60)*1000, SubSeverity.Type.NULL)//14 days
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, 0, SubSeverity.Type.PERM) //Perm
			})
	}),
	GAMEPLAY_OFFENCE(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, ((1*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, ((2*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, ((3*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, ((4*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, ((5*60)*60)*1000, SubSeverity.Type.NULL)
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, ((6*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, ((12*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, ((24*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, ((48*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, ((96*60)*60)*1000, SubSeverity.Type.NULL)
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, (((14*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, (((21*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, (((28*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, 0, SubSeverity.Type.PERM)
			}),
	}),
	CLIENT_MODIFICATION(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, ((6*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, ((12*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, ((24*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, ((48*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, ((96*60)*60)*1000, SubSeverity.Type.NULL)
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, (((2*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, (((4*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, (((6*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, (((8*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, (((10*24)*60)*60)*1000, SubSeverity.Type.NULL)
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(2, (((14*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(3, (((21*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(4, (((28*24)*60)*60)*1000, SubSeverity.Type.NULL),
					new SubSeverity(5, 0, SubSeverity.Type.PERM)
			}),
	}),
	INAPPROPRIATE_SKIN(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (30*60)*1000, SubSeverity.Type.NULL)})}),//30 Mins
	TPA_SPAM(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (30*60)*1000, SubSeverity.Type.NULL)})}),//30 Mins
	JOIN_ANNOUNCEMENT(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (30*60)*1000, SubSeverity.Type.NULL)})}),//30 Mins
	CLEAR_CREATIVE_INVENTORY(null),
	MASS_OR_FALSE_REPORTS(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (30*60)*1000, SubSeverity.Type.NULL)})}),//30 Mins
	FAILED_STAFF_REVIEW(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (((7*24)*60)*60)*1000, SubSeverity.Type.NULL)})}),//7 Days
	CLEAR_ALL_PUNISHMENTS(null),
	BAN_PERM(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, 0, SubSeverity.Type.PERM)})}),
	IPBAN_PERM(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, 0, SubSeverity.Type.PERM)})}),
	IPBAN(new Severity[] {new Severity(1, new SubSeverity[] {new SubSeverity(1, (((7*24)*60)*60)*1000, SubSeverity.Type.PERM)})}),
	UN_BAN(null),
	UN_MUTE(null),
	UN_BANIP(null);

	private Severity[] severities;

	PunishCategory(Severity[] severities)
	{
		this.severities = severities;
	}

	public Severity[] getSeverities()
	{
		return severities;
	}

	public Severity getSeverity(int index)
	{
		return severities[index-1];
	}

	public SubSeverity[] getSubSeverities(int index)
	{
		return severities[index-1].getSubSeverities();
	}

	public SubSeverity getSubSeverity(int index1, int index2)
	{
		return severities[index1-1].getSubSeverities()[index2-1];
	}
}
