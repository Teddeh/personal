package org.mccentral.punishment;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import lilypad.client.connect.api.request.impl.MessageRequest;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONObject;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.extra.BanCache;
import org.mccentral.punishment.cache.extra.MuteCache;
import org.mccentral.punishment.cache.extra.OtherCache;
import org.mccentral.punishment.lilypad.LilyMessenger;
import org.mccentral.punishment.lilypad.MessageChannel;
import org.mccentral.punishment.severity.SubSeverity;
import org.mccentral.punishment.util.C;
import org.mccentral.punishment.util.Callback;
import org.mccentral.punishment.util.json.JsonBuilder;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created: 16/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishManager implements Listener
{
	public HashMap<UUID, PlayerCache> cache;
	private Punishment plugin;

	public PunishManager(Punishment plugin)
	{
		this.plugin = plugin;
		this.cache = new HashMap<UUID, PlayerCache>();
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	public void mute(OfflinePlayer offlinePlayer, PunishCategory punishCategory, int severity, String reason, UUID staff)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if (connection == null || connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					public void call(Connection value)
					{
						mute(offlinePlayer, punishCategory, severity, reason, staff);
					}
				});
				return;
			}

			int subSev = 1;
			PreparedStatement statement = null;

			final String select = "select CATEGORY, SEVERITY, SUB_SEVERITY from " + plugin.getMySQL().getTable() + " where UUID=?;";
			statement = connection.prepareStatement(select);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			ResultSet result = statement.executeQuery();
			while (result.next())
			{
				if (!result.getString("CATEGORY").equals(punishCategory.toString())) continue;
				if (result.getInt("SEVERITY") != severity) continue;
				subSev += 1;
			}

			if (subSev > 5) subSev = 5;
			SubSeverity[] subSeverity = punishCategory.getSubSeverities(severity);
			if(subSeverity.length < subSev) subSev = subSeverity.length;
			long time = punishCategory.getSubSeverity(severity, subSev).getTime();
			Timestamp now = new Timestamp(System.currentTimeMillis()), then = new Timestamp(System.currentTimeMillis() + time);

			final String insert = "insert into " + plugin.getMySQL().getTable() + " (UUID, CATEGORY, SEVERITY, SUB_SEVERITY, IP, TIME, EXPIRE_TIME, STAFF, REASON) values (?,?,?,?,?,?,?,?,?);";
			statement = connection.prepareStatement(insert);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			statement.setString(2, punishCategory.toString());
			statement.setByte(3, (byte) severity);
			statement.setByte(4, (byte) subSev);
			statement.setString(5, offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL");
			statement.setTimestamp(6, now);
			statement.setTimestamp(7, then);
			statement.setString(8, staff.toString());
			statement.setString(9, reason);
			statement.execute();

			statement.close();

			MessageChannel channel = MessageChannel.PUNISH;
			JsonBuilder json = new JsonBuilder("type", channel.toString())
					.append("id", "0")
					.append("victim", offlinePlayer.getUniqueId().toString())
					.append("category", punishCategory.toString())
					.append("severity", String.valueOf(severity))
					.append("subSeverity", String.valueOf(subSev))
					.append("ip", offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL")
					.append("time", String.valueOf(now.getTime()))
					.append("expire", String.valueOf(then.getTime()))
					.append("staff", staff.toString())
					.append("reason", reason);

			plugin.getLilyMessenger().request(json, channel);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void ban(OfflinePlayer offlinePlayer, PunishCategory punishCategory, int severity, String reason, UUID staff)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if (connection == null || connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					public void call(Connection value)
					{
						ban(offlinePlayer, punishCategory, severity, reason, staff);
					}
				});
				return;
			}

			int subSev = 1;
			PreparedStatement statement = null;

			final String select = "select CATEGORY, SEVERITY, SUB_SEVERITY from " + plugin.getMySQL().getTable() + " where UUID=?;";
			statement = connection.prepareStatement(select);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			ResultSet result = statement.executeQuery();
			while (result.next())
			{
				if (!result.getString("CATEGORY").equals(punishCategory.toString())) continue;
				if (result.getInt("SEVERITY") != severity) continue;
				subSev += 1;
			}

			if (subSev > 5) subSev = 5;
			SubSeverity[] subSeverity = punishCategory.getSubSeverities(severity);
			if(subSeverity.length < subSev) subSev = subSeverity.length;
			long time = punishCategory.getSubSeverity(severity, subSev).getTime();
			Timestamp now = new Timestamp(System.currentTimeMillis()), then = new Timestamp(System.currentTimeMillis() + time);

			final String insert = "insert into " + plugin.getMySQL().getTable() + " (UUID, CATEGORY, SEVERITY, SUB_SEVERITY, IP, TIME, EXPIRE_TIME, STAFF, REASON) values (?,?,?,?,?,?,?,?,?);";
			statement = connection.prepareStatement(insert);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			statement.setString(2, punishCategory.toString());
			statement.setByte(3, (byte) severity);
			statement.setByte(4, (byte) subSev);
			statement.setString(5, offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL");
			statement.setTimestamp(6, now);
			statement.setTimestamp(7, then);
			statement.setString(8, staff.toString());
			statement.setString(9, reason);
			statement.execute();

			statement.close();

			MessageChannel channel = MessageChannel.PUNISH;
			JsonBuilder json = new JsonBuilder("type", channel.toString())
					.append("id", "0")
					.append("victim", offlinePlayer.getUniqueId().toString())
					.append("category", punishCategory.toString())
					.append("severity", String.valueOf(severity))
					.append("subSeverity", String.valueOf(subSev))
					.append("ip", offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL")
					.append("time", String.valueOf(now.getTime()))
					.append("expire", String.valueOf(then.getTime()))
					.append("staff", staff.toString())
					.append("reason", reason);

			plugin.getLilyMessenger().request(json, channel);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void other(OfflinePlayer offlinePlayer, PunishCategory punishCategory, int severity, String reason, UUID staff)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if (connection == null || connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					public void call(Connection value)
					{
						other(offlinePlayer, punishCategory, severity, reason, staff);
					}
				});
				return;
			}

			int subSev = 1;
			PreparedStatement statement = null;

			final String select = "select CATEGORY, SEVERITY, SUB_SEVERITY from " + plugin.getMySQL().getTable() + " where UUID=?;";
			statement = connection.prepareStatement(select);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			ResultSet result = statement.executeQuery();
			while (result.next())
			{
				if (!result.getString("CATEGORY").equals(punishCategory.toString())) continue;
				if (result.getInt("SEVERITY") != severity) continue;
				subSev += 1;
			}

			if (subSev > 5) subSev = 5;
			SubSeverity[] subSeverity = punishCategory.getSubSeverities(severity);
			if(subSeverity.length < subSev) subSev = subSeverity.length;
			long time = punishCategory.getSubSeverity(severity, subSev).getTime();
			Timestamp now = new Timestamp(System.currentTimeMillis()), then = new Timestamp(System.currentTimeMillis() + time);

			final String insert = "insert into " + plugin.getMySQL().getTable() + " (UUID, CATEGORY, SEVERITY, SUB_SEVERITY, IP, TIME, EXPIRE_TIME, STAFF, REASON) values (?,?,?,?,?,?,?,?,?);";
			statement = connection.prepareStatement(insert);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			statement.setString(2, punishCategory.toString());
			statement.setByte(3, (byte) severity);
			statement.setByte(4, (byte) subSev);
			statement.setString(5, offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL");
			statement.setTimestamp(6, now);
			statement.setTimestamp(7, then);
			statement.setString(8, staff.toString());
			statement.setString(9, reason);
			statement.execute();

			statement.close();

			MessageChannel channel = MessageChannel.PUNISH;
			JsonBuilder json = new JsonBuilder("type", channel.toString())
					.append("id", "0")
					.append("victim", offlinePlayer.getUniqueId().toString())
					.append("category", punishCategory.toString())
					.append("severity", String.valueOf(severity))
					.append("subSeverity", String.valueOf(subSev))
					.append("ip", offlinePlayer.isOnline() ? ((Player) offlinePlayer).getAddress().getHostName() : "NULL")
					.append("time", String.valueOf(now.getTime()))
					.append("expire", String.valueOf(then.getTime()))
					.append("staff", staff.toString())
					.append("reason", reason);

			plugin.getLilyMessenger().request(json, channel);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void clearPunishments(OfflinePlayer offlinePlayer, UUID staff)
	{

		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if (connection == null || connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					public void call(Connection value)
					{
						clearPunishments(offlinePlayer, staff);
					}
				});
				return;
			}

			final String insert = "delete from " + plugin.getMySQL().getTable() + " where UUID=?";
			PreparedStatement statement = connection.prepareStatement(insert);
			statement.setString(1, offlinePlayer.getUniqueId().toString());
			statement.execute();

			statement.close();

			MessageChannel channel = MessageChannel.CLEAR;
			JsonBuilder json = new JsonBuilder("type", channel.toString())
					.append("victim", offlinePlayer.getUniqueId().toString())
					.append("staff", staff.toString());

			plugin.getLilyMessenger().request(json, channel);

			Bukkit.getPlayer(staff).sendMessage("Cleared all punishments for " + offlinePlayer.getName());
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void unPunish(Player staff, String key, PunishCategory punishCategory)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if (connection == null || connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					public void call(Connection value)
					{
						unPunish(staff, key, punishCategory);
					}
				});
				return;
			}

			boolean ip = isIP(key);
			final String update = "update " + plugin.getMySQL().getTable() + " set `EXPIRE_TIME`=? where " + (ip ? "IP" : "UUID") + "=? and CATEGORY=?";
			PreparedStatement statement = null;

			if(punishCategory == PunishCategory.UN_BAN)
			{
				for(PunishCategory category : new PunishCategory[] {PunishCategory.BAN_PERM, PunishCategory.CLIENT_MODIFICATION, PunishCategory.GAMEPLAY_OFFENCE, PunishCategory.INAPPROPRIATE_SKIN, PunishCategory.JOIN_ANNOUNCEMENT, PunishCategory.TPA_SPAM, PunishCategory.MASS_OR_FALSE_REPORTS}) {
					statement = connection.prepareStatement(update);
					statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()-1000));
					statement.setString(2, key);
					statement.setString(3, category.toString());
					statement.execute();
				}
			}

			if(punishCategory == PunishCategory.UN_MUTE)
			{
				for(PunishCategory category : new PunishCategory[] {PunishCategory.CHAT_OFFENCE}) {
					statement = connection.prepareStatement(update);
					statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()-1000));
					statement.setString(2, key);
					statement.setString(3, category.toString());
					statement.execute();
				}
			}

			if(punishCategory == PunishCategory.UN_BANIP)
			{
				for(PunishCategory category : new PunishCategory[] {PunishCategory.IPBAN, punishCategory.IPBAN_PERM}) {
					statement = connection.prepareStatement(update);
					statement.setTimestamp(1, new Timestamp(System.currentTimeMillis() - 1000));
					statement.setString(2, key);
					statement.setString(3, category.toString());
					statement.execute();
				}
			}

			statement.close();

			MessageChannel channel = MessageChannel.OTHER;
			JsonBuilder json = new JsonBuilder("type", channel.toString())
					.append("ip", key)
					.append("isIP", String.valueOf(ip))
					.append("victim", key)
					.append("category", punishCategory.toString())
					.append("staff", staff.getName().toString());

			plugin.getLilyMessenger().request(json, channel);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return;
	}

	public void report(OfflinePlayer offlinePlayer, String reason, Player reporter)
	{

		MessageChannel channel = MessageChannel.REPORT_STAFF;
		JsonBuilder json = new JsonBuilder("type", channel.toString())
				.append("victim", offlinePlayer.getUniqueId().toString())
				.append("reason", reason)
				.append("reporter", reporter.getUniqueId().toString());

		plugin.getLilyMessenger().request(json, channel);

		reporter.sendMessage(" ");
		reporter.sendMessage(C.Red + "Report > " + C.Green + "Report successful.");
		reporter.sendMessage(C.Gray + "Mass reporting or Fake reports will result in a Ban.");
	}

	public void warn(OfflinePlayer offlinePlayer, String reason, UUID staff)
	{
		MessageChannel channel = MessageChannel.WARNING;
		JsonBuilder json = new JsonBuilder("type", channel.toString())
				.append("victim", offlinePlayer.getUniqueId().toString())
				.append("reason", reason)
				.append("reporter", staff.toString());

		plugin.getLilyMessenger().request(json, channel);

		Player player = Bukkit.getPlayer(staff);
		player.sendMessage(C.Red + "Punishment " + C.White + "Warning sent successfully.");
	}

	public void kick(OfflinePlayer offlinePlayer, String reason, UUID staff)
	{
		MessageChannel channel = MessageChannel.KICK;
		JsonBuilder json = new JsonBuilder("type", channel.toString())
				.append("victim", offlinePlayer.getUniqueId().toString())
				.append("reason", reason)
				.append("reporter", staff.toString());

		plugin.getLilyMessenger().request(json, channel);
	}

	private String getBanMessage(PunishCategory category, int severity, int subSeverity, String reason, long time, long expireTime, String staff, int id)
	{
		return JSONObject.escape(C.RedB + "You are Banned" + C.Reset + "\n"
				+ C.White + "Reason: " + C.Gray + reason + "\n"
				+ C.White + "Expires: " + getCalculation(category, severity, subSeverity, expireTime) + C.Reset + "\n"
				+ C.White + "Punishment ID: " + C.RedB + id + C.Reset + "\n\n"
				+ C.Yellow + "Believe your punishment was a mistake?" + "\n"
				+ C.Yellow + "Post an appeal at: " + C.Gold + "www.mccentral.org/appealpreview");
	}

	public String getCalculation(PunishCategory category, int severity, int subSeverity, long time)
	{
		if(category.getSubSeverity(severity, subSeverity).getType() == SubSeverity.Type.PERM)
			return C.Red + "Permanent " + C.White + "(" + C.Gray + "Never" + C.White + ")";

		long diff = time - System.currentTimeMillis();
		long d = diff / (1000 * 60 * 60 * 24);
		long h = diff / (60 * 60 * 1000) % 24;
		long m = diff / (60 * 1000) % 60;
		long s = diff / 1000 % 60;

		return(!(d <= 0) ? C.Gold + d + C.Yellow + " Day" + (d > 1 ? "s" : "") + C.White + ", " : "")
				+ (!(h <= 0) ? C.Gold + h + C.Yellow + " Hour" + (h > 1 ? "s" : "") + C.White + ", " : "")
				+ (!(m <= 0) ? C.Gold + m + C.Yellow + " Minute" + (m > 1 ? "s" : "") + C.White + ", " : "")
				+ (!(s <= 0) ? C.Gold + s + C.Yellow + " Second" + (s > 1 ? "s" : "") : "");
	}

	public boolean isIP(String text) {
		Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
		Matcher m = p.matcher(text);
		return m.find();
	}

	@EventHandler
	public void onJoin(AsyncPlayerPreLoginEvent event)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			final String select = "select * from " + plugin.getMySQL().getTable() + " where UUID=?";
			PreparedStatement statement = connection.prepareStatement(select);
			statement.setString(1, event.getUniqueId().toString());
			ResultSet result = statement.executeQuery();

			List<BanCache> bans = new ArrayList<>();
			List<MuteCache> mutes = new ArrayList<>();
			List<OtherCache> other = new ArrayList<>();

			while(result.next())
			{
				PunishCategory category = PunishCategory.valueOf(result.getString("CATEGORY"));
				final int severity = result.getInt("SEVERITY"), subSeverity = result.getInt("SUB_SEVERITY"), id = result.getInt("ID");
				final Timestamp time = result.getTimestamp("TIME"), expire = result.getTimestamp("EXPIRE_TIME");
				final String staff = result.getString("STAFF"), ip = result.getString("IP"), reason = result.getString("REASON");
				if(category == PunishCategory.GAMEPLAY_OFFENCE || category == PunishCategory.CLIENT_MODIFICATION || category == PunishCategory.BAN_PERM || category == PunishCategory.INAPPROPRIATE_SKIN || category == PunishCategory.TPA_SPAM || category == PunishCategory.JOIN_ANNOUNCEMENT || category == PunishCategory.MASS_OR_FALSE_REPORTS || category == PunishCategory.FAILED_STAFF_REVIEW) {
					bans.add(new BanCache(category, id, severity, subSeverity, time.getTime(), expire.getTime(), reason, staff));
				}
				else if(category == PunishCategory.CHAT_OFFENCE) {
					mutes.add(new MuteCache(category, id, severity, subSeverity, time.getTime(), expire.getTime(), reason, staff));
				}
				else {
					other.add(new OtherCache(category, id, time.getTime(), staff, reason));
				}
			}

			for(BanCache cache : bans)
			{
				if(cache.getCategory() == PunishCategory.FAILED_STAFF_REVIEW) continue;
				if(cache.getCategory().getSubSeverity(cache.getSeverity(), cache.getSubSeverity()).getType() == SubSeverity.Type.PERM)
				{
					//Perm ban
					event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, getBanMessage(cache.getCategory(), cache.getSeverity(), cache.getSubSeverity(), cache.getReason(), cache.getTimeBanned(), cache.getExpireTime(), cache.getStaffUUID(), cache.getId()));
					break;
				}

				if(cache.getExpireTime() < System.currentTimeMillis()) continue;
				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, getBanMessage(cache.getCategory(), cache.getSeverity(), cache.getSubSeverity(), cache.getReason(), cache.getTimeBanned(), cache.getExpireTime(), cache.getStaffUUID(), cache.getId()));
				return;
			}

			cache.put(event.getUniqueId(), new PlayerCache(bans, mutes, other));
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event)
	{
		if(cache.containsKey(event.getPlayer().getUniqueId())) cache.remove(event.getPlayer().getUniqueId());
	}

	@EventHandler (ignoreCancelled = true)
	public void playerChat(AsyncPlayerChatEvent event)
	{
		if(!cache.containsKey(event.getPlayer().getUniqueId())) return;
		PlayerCache playerCache = cache.get(event.getPlayer().getUniqueId());
		for(MuteCache muteCache : playerCache.getMuteCache())
		{
			if(muteCache.getCategory().getSubSeverity(muteCache.getSeverity(), muteCache.getSubSeverity()).getType() == SubSeverity.Type.PERM)
			{
				event.setCancelled(true);
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(C.RedB + "You are Muted!");
				event.getPlayer().sendMessage(C.White + "Reason: " + C.Gray + muteCache.getReason());
				event.getPlayer().sendMessage(C.White + "Expires: " + getCalculation(muteCache.getCategory(), muteCache.getSeverity(), muteCache.getSubSeverity(), muteCache.getExpireTime()));
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(C.Yellow + "Believe your punishment was a mistake?");
				event.getPlayer().sendMessage(C.Yellow + "Post an appeal at: " + C.Gold + "www.mccentral.org/appealpreview");
				break;
			}

			if(!muteCache.isActive()) continue;
			if(muteCache.getExpireTime() < System.currentTimeMillis()) muteCache.setActive(false);
			if(muteCache.isActive())
			{
				event.setCancelled(true);
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(C.RedB + "You are Muted!");
				event.getPlayer().sendMessage(C.White + "Reason: " + C.Gray + muteCache.getReason());
				event.getPlayer().sendMessage(C.White + "Expires: " + getCalculation(muteCache.getCategory(), muteCache.getSeverity(), muteCache.getSubSeverity(), muteCache.getExpireTime()));
				event.getPlayer().sendMessage(" ");
				event.getPlayer().sendMessage(C.Yellow + "Believe your punishment was a mistake?");
				event.getPlayer().sendMessage(C.Yellow + "Post an appeal at: " + C.Gold + "www.mccentral.org/appealpreview");
				break;
			}
		}
	}
}
