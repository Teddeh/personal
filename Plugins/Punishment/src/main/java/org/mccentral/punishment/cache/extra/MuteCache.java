package org.mccentral.punishment.cache.extra;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.util.C;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MuteCache extends PunishCache
{
	private int severity, subSeverity;
	private long timeMuted, expireTime;
	private String reason, staffUUID;
	private boolean active;

	public MuteCache(PunishCategory category, int id, int severity, int subSeverity, long timeMuted, long expireTime, String reason, String staffUUID)
	{
		super(category, id);

		this.severity = severity;
		this.subSeverity = subSeverity;
		this.timeMuted = timeMuted;
		this.expireTime = expireTime;
		this.reason = reason;
		this.staffUUID = staffUUID;

		if(expireTime <= 0) {
			this.active = true;
			return;
		}

		this.active = expireTime > System.currentTimeMillis();
	}

	public int getSeverity()
	{
		return severity;
	}

	public String getSeverityString()
	{
		if(severity == 1) return C.GreenB + severity;
		if(severity == 2) return C.YellowB + severity;
		if(severity == 3) return C.RedB + severity;
		return ""+severity;
	}

	public int getSubSeverity()
	{
		return subSeverity;
	}

	public long getTimeMuted()
	{
		return timeMuted;
	}

	public long getExpireTime()
	{
		return expireTime;
	}

	public String getReason()
	{
		return reason;
	}

	public String getStaffUUID()
	{
		return staffUUID;
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}
}
