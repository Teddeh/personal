package org.mccentral.punishment.cache.extra;

import org.mccentral.punishment.PunishCategory;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class OtherCache extends PunishCache
{
	private long timeOccured;
	private String staffUUID, reason;

	public OtherCache(PunishCategory category, int id, long timeOccured, String staffUUID, String reason)
	{
		super(category, id);

		this.timeOccured = timeOccured;
		this.staffUUID = staffUUID;
		this.reason = reason;
	}

	public long getTimeOccured()
	{
		return timeOccured;
	}

	public String getStaffUUID()
	{
		return staffUUID;
	}

	public String getReason()
	{
		return reason;
	}
}
