package org.mccentral.punishment.command;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishPermissions;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.util.C;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ReportCommand implements CommandExecutor
{
	private PunishManager punishManager;
	private Punishment plugin;

	public ReportCommand(Punishment plugin, PunishManager punishManager)
	{
		this.plugin = plugin;
		this.punishManager = punishManager;

		plugin.getCommand("report").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
	{
		if (!(commandSender instanceof Player)) return true;

		Player sender = (Player) commandSender;

		if (!sender.hasPermission(PunishPermissions.REPORT_ISSUE.getPermission()) && !sender.isOp())
		{
			sender.sendMessage(C.Red + "You don't have permission for this.");
			return true;
		}

		if (args.length < 2)
		{
			sender.sendMessage(C.Red + "Report > " + C.White + "Invalid arguments, /report <player> <reason>");
			return true;
		}

		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
		if (offlinePlayer == null)
		{
			sender.sendMessage(C.Red + "Report > " + C.White + "The player, " + args[0] + " was not recognised.");
			return true;
		}

		StringBuilder reason = new StringBuilder();
		for (int i = 1; i < args.length; i++)
		{
			reason.append(args[i]).append(" ");
		}

		punishManager.report(offlinePlayer, reason.toString(), sender);
		return true;
	}
}
