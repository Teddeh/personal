package org.mccentral.punishment.inventory;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishPermissions;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.extra.BanCache;
import org.mccentral.punishment.cache.extra.MuteCache;
import org.mccentral.punishment.cache.extra.OtherCache;
import org.mccentral.punishment.cache.extra.PunishCache;
import org.mccentral.punishment.inventory.api.CentralMenu;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.C;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.SkullBuilder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishMenu extends CentralMenu
{
	private PlayerCache playerCache;
	private PunishManager punishManager;

	public PunishMenu(Punishment plugin, OfflinePlayer offlinePlayer, PunishManager punishManager, String reason,
	                  Player staff, final PlayerCache playerCache, String ip, boolean isIP)
	{
		super(plugin, "Punishment Menu", 6, true, true);
		this.punishManager = punishManager;
		this.playerCache = playerCache;

		if(!isIP && offlinePlayer == null)
		{
			staff.sendMessage(C.Red + "This player has never played this server before,");
			return;
		}

		//If playerCache is null, create one but not store it.
		if(playerCache == null)
		{
			new BukkitRunnable()
			{
				@Override
				public void run()
				{
					try
					{
						Connection connection = plugin.getMySQL().getConnection();
						String key = (isIP ? "IP" : "UUID");
						String select = "select * from " + plugin.getMySQL().getTable() + " where " + key + "=?";
						PreparedStatement statement = connection.prepareStatement(select);
						statement.setString(1, isIP ? ip : offlinePlayer.getUniqueId().toString());
						ResultSet result = statement.executeQuery();

						List<BanCache> bans = new ArrayList<>();
						List<MuteCache> mutes = new ArrayList<>();
						List<OtherCache> other = new ArrayList<>();

						while (result.next())
						{
							PunishCategory category = PunishCategory.valueOf(result.getString("CATEGORY"));
							final int severity = result.getInt("SEVERITY"), subSeverity = result.getInt("SUB_SEVERITY"), id = result.getInt("ID");
							final long time = result.getLong("TIME"), expire = result.getLong("EXPIRE_TIME");
							final String staff2 = result.getString("STAFF"), ip = result.getString("IP"), reason2 = result.getString("REASON");
							if (category == PunishCategory.GAMEPLAY_OFFENCE || category == PunishCategory.CLIENT_MODIFICATION)
							{
								bans.add(new BanCache(category, id, severity, subSeverity, time, expire, reason2, staff2));
							} else if (category == PunishCategory.CHAT_OFFENCE)
							{
								mutes.add(new MuteCache(category, id, severity, subSeverity, time, expire, reason2, staff2));
							} else
							{
								other.add(new OtherCache(category, id, time, staff2, reason));
							}
						}

						PunishMenu.this.playerCache = new PlayerCache(bans, mutes, other);

						statement.close();
						result.close();

					}

					catch(SQLException e)

					{
						e.printStackTrace();
					}
				}
			}.runTaskAsynchronously(plugin);
		}

		final PlayerCache newCache = playerCache;

		//Head
		String headName = (isIP ? ip : offlinePlayer.getName());
		ItemStack skull = new SkullBuilder().setOwner(headName).setLore(C.White + reason).build();
		addItem(new MenuItem(4, skull));

		//Chat Offence
		ItemStack chatOffence = new ItemStackBuilder(Material.IRON_SWORD).setName(C.WhiteB + "Chat Offences").setLore(C.GrayI + "Punishment Type: Mute", "", C.Yellow + "Total Chat punishments: " + C.Gold + newCache.getTimesMuted(-1)).build();
		addItem(new MenuItem(11, chatOffence));
		addItem(new MenuItem(20, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5).setName(C.Green + "Severity " + C.GreenB + "1" + C.Reset).setLore("", C.White + "1st Offence: 15 Minutes", C.White + "2nd Offence: 30 Minutes", C.White + "3rd Offence: 45 Minutes", C.White + "4th Offence: 60 Minutes", C.White + "5th+ Offence: 75 Minutes", "", C.Gray + "Total Sev.1 punishments: " + C.Yellow + newCache.getTimesMuted(1)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_CHATOFFENCE_SEV1.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.mute(offlinePlayer, PunishCategory.CHAT_OFFENCE, 1, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(29, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4).setName(C.Yellow + "Severity " + C.YellowB + "2" + C.Reset).setLore("", C.White + "1st Offence: 2 Days", C.White + "2nd Offence: 4 Days", C.White + "3rd Offence: 5 Days", C.White + "4th Offence: 7 Days", C.White + "5th+ Offence: 14 Days", "", C.Gray + "Total Sev.2 punishments: " + C.Yellow + newCache.getTimesMuted(2)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_CHATOFFENCE_SEV2.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.mute(offlinePlayer, PunishCategory.CHAT_OFFENCE, 2, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(38, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14).setName(C.Red + "Severity " + C.RedB + "3" + C.Reset).setLore("", C.White + "1st Offence: Permanent").build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_CHATOFFENCE_SEV3.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.mute(offlinePlayer, PunishCategory.CHAT_OFFENCE, 3, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});

		//Gameplay Offence
		ItemStack gameplayOffence = new ItemStackBuilder(Material.GOLD_SWORD).setName(C.WhiteB + "Gameplay Offences").setLore(C.GrayI + "Punishment Type: Ban", "", C.Yellow + "Total Gameplay punishments: " + C.Gold + newCache.getTimesBanned(-1, PunishCategory.GAMEPLAY_OFFENCE)).build();
		addItem(new MenuItem(13, gameplayOffence));
		addItem(new MenuItem(22, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5).setName(C.Green + "Severity " + C.GreenB + "1" + C.Reset).setLore("", C.White + "1st Offence: 1 Hour", C.White + "2nd Offence: 2 Hours", C.White + "3rd Offence: 3 Hours", C.White + "4th Offence: 4 Hours", C.White + "5th+ Offence: 5 Hours", "", C.Gray + "Total Sev.1 punishments: " + C.Yellow + newCache.getTimesBanned(1, PunishCategory.GAMEPLAY_OFFENCE)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_GAMEPLAY_SEV1.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.GAMEPLAY_OFFENCE, 1, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(31, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4).setName(C.Yellow + "Severity " + C.YellowB + "2" + C.Reset).setLore("", C.White + "1st Offence: 6 Hours", C.White + "2nd Offence: 12 Hours", C.White + "3rd Offence: 24 Hours", C.White + "4th Offence: 48 Hours", C.White + "5th+ Offence: 96 Hours", "", C.Gray + "Total Sev.2 punishments: " + C.Yellow + newCache.getTimesBanned(2, PunishCategory.GAMEPLAY_OFFENCE)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_GAMEPLAY_SEV2.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.GAMEPLAY_OFFENCE, 2, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(40, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14).setName(C.Red + "Severity " + C.RedB + "3" + C.Reset).setLore("", C.White + "1st Offence: 7 Days", C.White + "2nd Offence: 14 Days", C.White + "3rd Offence: 21 Days", C.White + "4th Offence: 28 Days", C.White + "5th+ Offence: Permanent", "", C.Gray + "Total Sev.3 punishments: " + C.Yellow + newCache.getTimesBanned(3, PunishCategory.GAMEPLAY_OFFENCE)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(punishManager.cache.containsKey(player.getUniqueId())) {
					if(!punishManager.cache.get(player.getUniqueId()).canIssueSevThree()) {
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						player.closeInventory();
						return;
					}
				}

				if(!player.hasPermission(PunishPermissions.PUNISH_GAMEPLAY_SEV3.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.GAMEPLAY_OFFENCE, 3, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});

		//Client Modification
		ItemStack clientModOffence = new ItemStackBuilder(Material.DIAMOND_SWORD).setName(C.WhiteB + "Client Modifications").setLore(C.GrayI + "Punishment Type: Ban", "", C.Yellow + "Total Hack punishments: " + C.Gold + newCache.getTimesBanned(-1, PunishCategory.CLIENT_MODIFICATION)).build();
		addItem(new MenuItem(15, clientModOffence));
		addItem(new MenuItem(24, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5).setName(C.Green + "Severity " + C.GreenB + "1" + C.Reset).setLore(C.White + "clientMod offence subSev. 1").setLore("", C.White + "1st Offence: 6 Hours", C.White + "2nd Offence: 12 Hours", C.White + "3rd Offence: 24 Hours", C.White + "4th Offence: 48 Hours", C.White + "5th+ Offence: 96 Hours", "", C.Gray + "Total Sev.1 punishments: " + C.Yellow + newCache.getTimesBanned(1, PunishCategory.CLIENT_MODIFICATION)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_CLIENTMODS_SEV1.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.CLIENT_MODIFICATION, 1, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(33, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4).setName(C.Yellow + "Severity " + C.YellowB + "2" + C.Reset).setLore(C.White + "clientMod offence subSev. 2").setLore("", C.White + "1st Offence: 2 Days", C.White + "2nd Offence: 4 Days", C.White + "3rd Offence: 6 Days", C.White + "4th Offence: 8 Days", C.White + "5th+ Offence: 10 Days", "", C.Gray + "Total Sev.2 punishments: " + C.Yellow + newCache.getTimesBanned(2, PunishCategory.CLIENT_MODIFICATION)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PUNISH_CLIENTMODS_SEV2.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.CLIENT_MODIFICATION, 2, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(42, new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14).setName(C.Red + "Severity " + C.RedB + "3" + C.Reset).setLore(C.White + "clientMod offence subSev. 3").setLore("", C.White + "1st Offence: 7 Days", C.White + "2nd Offence: 14 Days", C.White + "3rd Offence: 21 Days", C.White + "4th Offence: 28 Days", C.White + "5th+ Offence: Permanent", "", C.Gray + "Total Sev.3 punishments: " + C.Yellow + newCache.getTimesBanned(3, PunishCategory.CLIENT_MODIFICATION)).build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(punishManager.cache.containsKey(player.getUniqueId())) {
					if(!punishManager.cache.get(player.getUniqueId()).canIssueSevThree()) {
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						player.closeInventory();
						return;
					}
				}

				if(!player.hasPermission(PunishPermissions.PUNISH_CLIENTMODS_SEV3.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.CLIENT_MODIFICATION, 3, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});

		//Addons
		addItem(new MenuItem(17, new ItemStackBuilder(Material.PAPER).setName(C.Red + "Warning").build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.WARN.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.warn(offlinePlayer, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(26, new ItemStackBuilder(Material.LEATHER_BOOTS).setName(C.Red + "Kick").build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.KICK.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.kick(offlinePlayer, reason, staff.getUniqueId());
				player.closeInventory();
			}
		});
		addItem(new MenuItem(35, new ItemStackBuilder(Material.COMMAND).setName(C.Red + "More Options").build()) {
			public void click(Player player, ClickType clickType) {
				if(!player.hasPermission(PunishPermissions.OTHER_OPTIONS.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				player.closeInventory();
				new MoreOptionsMenu(plugin, offlinePlayer, punishManager, reason, staff, newCache, ip, isIP).openInventory(player);
			}
		});

		addItem(new MenuItem(9, new ItemStackBuilder(Material.BARRIER).setName(C.Red + "Permanent Ban").build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				if(!player.hasPermission(PunishPermissions.PERM_BAN.getPermission()) && !player.isOp())
				{
					player.sendMessage(C.Red + "You don't have permission to perform this action.");
					return;
				}
				punishManager.ban(offlinePlayer, PunishCategory.BAN_PERM, 1, reason, player.getUniqueId());
				player.closeInventory();
			}
		});
			addItem(new MenuItem(18, new ItemStackBuilder(Material.REDSTONE).setName(C.Red + "Temporary IP Ban").build()) {
			public void click(Player player, ClickType clickType) {

				player.sendMessage("Todo");
			}
		});
		addItem(new MenuItem(27, new ItemStackBuilder(Material.REDSTONE_BLOCK).setName(C.Red + "Permanent IP Ban").build()) {
			public void click(Player player, ClickType clickType) {

				player.sendMessage("Todo");
			}
		});

		//Recent offences
		addItem(new MenuItem(53, new ItemStackBuilder(Material.BOOK).setName(C.White + "Show ALL Offences").build()) {
			public void click(Player player, ClickType clickType) {
				if(isIP) return;
				player.closeInventory();
				new AllPunishmentsMenu(plugin, offlinePlayer, punishManager, reason, staff, playerCache, ip, isIP).openInventory(player);
			}
		});

		if(isIP) return;

		int x = 1;
		for(int i = 45; i < 53; i++) {
			if(newCache.getPunishmentsInOrder().size() < x) continue;
			PunishCache punishCache = newCache.getPunishmentsInOrder().get(x-1);
			if(punishCache == null) continue;
			if(punishCache instanceof BanCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.Gray + ((BanCache) punishCache).getReason(), "", C.White + "Severity: " + ((BanCache) punishCache).getSeverityString() +  C.White + " (" +  C.Gray + ((BanCache) punishCache).getSubSeverity() +  C.White + ")", C.White + "Date: " +  C.Gray + new Timestamp(((BanCache) punishCache).getTimeBanned()).toString(), C.White + "Staff: " +  C.Gray + Bukkit.getOfflinePlayer(UUID.fromString(((BanCache) punishCache).getStaffUUID())).getName(), "", C.White + "ID: " + C.Red + punishCache.getId()).setAmount(x).build()));
			if(punishCache instanceof MuteCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.Gray + ((MuteCache) punishCache).getReason(), "", C.White + "Severity: " + ((MuteCache) punishCache).getSeverityString() +  C.White + " (" +  C.Gray + ((MuteCache) punishCache).getSubSeverity() +  C.White + ")", C.White + "Date: " +  C.Gray + new Timestamp(((MuteCache) punishCache).getTimeMuted()).toString(), C.White + "Staff: " +  C.Gray + Bukkit.getOfflinePlayer(UUID.fromString(((MuteCache) punishCache).getStaffUUID())).getName(), "", C.White + "ID: " +  C.Red + punishCache.getId()).setAmount(x).build()));
			if(punishCache instanceof OtherCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.White + "ID: " +  C.Red + punishCache.getId()).setAmount(x).build()));
			x += 1;
		}
	}

	private class MoreOptionsMenu extends CentralMenu
	{
		public MoreOptionsMenu(Punishment plugin, OfflinePlayer offlinePlayer, PunishManager punishManager, String reason, Player staff, PlayerCache playerCache, String ip, boolean isIP)
		{
			super(plugin, "More Options", 3, true, new PunishMenu(plugin, offlinePlayer, punishManager, reason, staff, playerCache, ip, isIP), true);

			//Head
			ItemStack skull = new SkullBuilder().setOwner(offlinePlayer.getName()).build();
			addItem(new MenuItem(4, skull));

			//Anvils
			addItem(new MenuItem(10, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Inappropriate skin").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.INAPPROPRIATE_SKIN.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.ban(offlinePlayer, PunishCategory.INAPPROPRIATE_SKIN, 1, reason, player.getUniqueId());
					player.closeInventory();
				}
			});
			addItem(new MenuItem(11, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Revoke TPA Privileges").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.TPA_SPAM.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.ban(offlinePlayer, PunishCategory.TPA_SPAM, 1, reason, player.getUniqueId());
					player.closeInventory();
				}
			});
			addItem(new MenuItem(12, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Revoke Join Announcement").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.JOIN_ANNOUNCEMENT.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.ban(offlinePlayer, PunishCategory.JOIN_ANNOUNCEMENT, 1, reason, player.getUniqueId());
					player.closeInventory();
				}
			});
			addItem(new MenuItem(13, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Clear Creative Inventory").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.CLEAR_INVENTORY.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}

					if(offlinePlayer == null || !offlinePlayer.isOnline())
					{
						player.sendMessage(C.Red + "This player cannot be found.");
						return;
					}

					Player victim = Bukkit.getPlayer(offlinePlayer.getUniqueId());
					victim.getInventory().clear();
					victim.updateInventory();

					victim.sendMessage(C.Yellow + "Your inventory has been cleared by, " + C.Gold + player.getName());
					player.sendMessage(C.Gold + victim.getName() + C.Yellow + "'s Inventory has been cleared.");
					player.closeInventory();
				}
			});
			addItem(new MenuItem(14, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Revoke Report Privileges").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.MASS_OR_FALSE_REPORTS.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.ban(offlinePlayer, PunishCategory.MASS_OR_FALSE_REPORTS, 1, reason, player.getUniqueId());
					player.closeInventory();
				}
			});
			addItem(new MenuItem(15, new ItemStackBuilder(Material.ANVIL).setName(C.White + "Clear All Punishments").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.PUNISH_CLEAR.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.clearPunishments(offlinePlayer, player.getUniqueId());
					player.closeInventory();
				}
			});
			addItem(new MenuItem(16, new ItemStackBuilder(Material.ANVIL).setName(C.Red + "Failed Staff Review").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP) return;
					if(!player.hasPermission(PunishPermissions.FAILED_STAFF_REVIEW.getPermission()) && !player.isOp())
					{
						player.sendMessage(C.Red + "You don't have permission to perform this action.");
						return;
					}
					punishManager.other(offlinePlayer, PunishCategory.FAILED_STAFF_REVIEW, 1, reason, staff.getUniqueId());
					player.closeInventory();
				}
			});

			addItem(new MenuItem(26, new ItemStackBuilder(Material.BOOK).setName(C.White + "Punishment Guidelines").build()) {
				public void click(Player player, ClickType clickType) {
					player.sendMessage(C.Green + "Punishment Guidelines");
					player.sendMessage(C.YellowB + " > " + C.Gold + "http://mccentral.org/guidelinesredirect");
					player.closeInventory();
				}
			});

			addItem(new MenuItem(21, new ItemStackBuilder(Material.INK_SACK, (byte) 1).setName(C.White + "Unban").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP)
						punishManager.unPunish(player, ip, PunishCategory.UN_BAN);
					else
						punishManager.unPunish(player, offlinePlayer.getUniqueId().toString(), PunishCategory.UN_BAN);
					player.closeInventory();
				}
			});
			addItem(new MenuItem(22, new ItemStackBuilder(Material.INK_SACK, (byte) 1).setName(C.White + "Unmute").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP)
						punishManager.unPunish(player, ip, PunishCategory.UN_MUTE);
					else
						punishManager.unPunish(player, offlinePlayer.getUniqueId().toString(), PunishCategory.UN_MUTE);
					player.closeInventory();
				}
			});
			addItem(new MenuItem(23, new ItemStackBuilder(Material.INK_SACK, (byte) 1).setName(C.White + "Unban Ip").build()) {
				public void click(Player player, ClickType clickType) {
					if(isIP)
						punishManager.unPunish(player, ip, PunishCategory.UN_BANIP);
					else
						punishManager.unPunish(player, offlinePlayer.getUniqueId().toString(), PunishCategory.UN_BANIP);
					player.closeInventory();
				}
			});

			//Back
			addItem(new MenuItem(18, new ItemStackBuilder(Material.ARROW).setName(C.Red + "Go Back").build()) {
				public void click(Player player, ClickType clickType) {
					player.closeInventory();
					new PunishMenu(plugin, offlinePlayer, punishManager, reason, staff, playerCache, ip, isIP).openInventory(player);
				}
			});
		}
	}

	private class AllPunishmentsMenu extends CentralMenu
	{

		public AllPunishmentsMenu(Punishment plugin, OfflinePlayer offlinePlayer, PunishManager punishManager, String reason, Player staff, PlayerCache playerCache, String ip, boolean isIP)
		{
			super(plugin, "All punishments", 6);

			//Head
			ItemStack skull = new SkullBuilder().setOwner(offlinePlayer.getName()).build();
			addItem(new MenuItem(49, skull));

			//Back
			addItem(new MenuItem(45, new ItemStackBuilder(Material.ARROW).setName(C.Red + "Go Back").build()) {
				public void click(Player player, ClickType clickType) {
					player.closeInventory();
					new PunishMenu(plugin, offlinePlayer, punishManager, reason, staff, playerCache, ip, isIP).openInventory(player);
				}
			});

			int[] blacklist = new int[] {45, 49};

			int x = 1;
			for(int i = 0; i < 54; i++)
			{
				for(Integer z : blacklist) if(i == z) break;

				if(playerCache.getPunishmentsInOrder().size() < x) continue;
				PunishCache punishCache = playerCache.getPunishmentsInOrder().get(x-1);
				if(punishCache == null) continue;
				if(punishCache instanceof BanCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.Gray + ((BanCache) punishCache).getReason(), "", C.White + "Severity: " + ((BanCache) punishCache).getSeverityString() +  C.White + " (" +  C.Gray + ((BanCache) punishCache).getSubSeverity() +  C.White + ")", C.White + "Date: " +  C.Gray + new Timestamp(((BanCache) punishCache).getTimeBanned()).toString(), C.White + "Staff: " +  C.Gray + Bukkit.getOfflinePlayer(UUID.fromString(((BanCache) punishCache).getStaffUUID())).getName(), "", C.White + "ID: " + C.Red + punishCache.getId()).setAmount(x).build()));
				if(punishCache instanceof MuteCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.Gray + ((MuteCache) punishCache).getReason(), "", C.White + "Severity: " + ((MuteCache) punishCache).getSeverityString() +  C.White + " (" +  C.Gray + ((MuteCache) punishCache).getSubSeverity() +  C.White + ")", C.White + "Date: " +  C.Gray + new Timestamp(((MuteCache) punishCache).getTimeMuted()).toString(), C.White + "Staff: " +  C.Gray + Bukkit.getOfflinePlayer(UUID.fromString(((MuteCache) punishCache).getStaffUUID())).getName(), "", C.White + "ID: " +  C.Red + punishCache.getId()).setAmount(x).build()));
				if(punishCache instanceof OtherCache) addItem(new MenuItem(i, new ItemStackBuilder(Material.PAPER).setName(C.WhiteB + punishCache.getCategory().toString().replace("_", " ")).setLore(C.Gray + ((OtherCache) punishCache).getReason(), "", C.White + "ID: " +  C.Red + punishCache.getId()).setAmount(x).build()));
				x += 1;
			}
		}
	}
}
