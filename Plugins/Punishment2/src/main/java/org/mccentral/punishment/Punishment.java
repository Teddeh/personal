package org.mccentral.punishment;

import lilypad.client.connect.api.Connect;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.punishment.command.PunishCommand;
import org.mccentral.punishment.command.ReportCommand;
import org.mccentral.punishment.database.MySQL;
import org.mccentral.punishment.inventory.api.MenuManager;
import org.mccentral.punishment.message.LilyMessenger;
import org.mccentral.punishment.util.Callback;

import java.sql.Connection;
import java.util.logging.Level;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Punishment extends JavaPlugin
{
	private MySQL mySQL;
	private Connect lilyConnect;
	private LilyMessenger lilyMessenger;

	@Override
	public void onEnable()
	{
		super.onEnable();

		mySQL = new MySQL(this, getConfig().getString("host"), getConfig().getString("port"), getConfig().getString("database"), getConfig().getString("table"), getConfig().getString("username"), getConfig().getString("password"));
		mySQL.openConnection(new Callback<Connection>() {
			public void call(Connection value) {
				System.out.print("SQL Connected");
				mySQL.register();//CREATE THE TABLE IF NOT EXISTS
			}
		});

		lilyConnect = getServer().getServicesManager().getRegistration(Connect.class).getProvider();
		connect();

		PunishManager punishManager = new PunishManager(this);
		lilyMessenger = new LilyMessenger(this, punishManager);
		lilyConnect.registerEvents(lilyMessenger);

		new MenuManager(this);
		new PunishCommand(this, punishManager);
		new ReportCommand(this, punishManager);
	}

	@Override
	public void onDisable()
	{
		super.onDisable();
	}

	private void connect()
	{
		try
		{
			lilyConnect.connect();
			getLogger().log(Level.INFO, "Lilypad Hook successful.");
		}
		catch (Throwable throwable)
		{
			getLogger().log(Level.SEVERE, getDescription().getName() + ":" + getDescription().getVersion() + " Could not connect to Lilypad Proxy.");
			throwable.printStackTrace();
		}
	}

	public MySQL getMySQL()
	{
		return mySQL;
	}

	public Connect getLilyConnect()
	{
		return lilyConnect;
	}

	public LilyMessenger getLilyMessenger()
	{
		return lilyMessenger;
	}
}
