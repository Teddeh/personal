package org.mccentral.punishment.cache.misc;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface SeverityCache
{
	int getSeverity();
	int getSubSeverity();
}
