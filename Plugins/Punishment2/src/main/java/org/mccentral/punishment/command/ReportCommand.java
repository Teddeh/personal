package org.mccentral.punishment.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishType;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.type.UnknownCache;
import org.mccentral.punishment.message.MessageChannel;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.JsonBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ReportCommand implements CommandExecutor
{
	private PunishManager punishManager;
	private Punishment plugin;

	public ReportCommand(Punishment plugin, PunishManager punishManager)
	{
		this.plugin = plugin;
		this.punishManager = punishManager;

		plugin.getCommand("report").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
	{
		if (!(commandSender instanceof Player)) return true;

		Player sender = (Player) commandSender;

		if(!punishManager.hasPerm(sender, PunishPermissions.REPORT_ISSUE)) return true;

		if (args.length < 2)
		{
			sender.sendMessage(Colour.Red + "Report > " + Colour.White + "Invalid arguments, /report <player> <reason>");
			return true;
		}

		if(args[0].equalsIgnoreCase(sender.getName()))
		{
			sender.sendMessage(Colour.Red + "You cannot report yourself silly..");
			return true;
		}

		StringBuilder reason = new StringBuilder();
		for (int i = 1; i < args.length; i++)
		{
			reason.append(args[i]).append(" ");
		}

		boolean canSend = true;
		UnknownCache unknownCache = null;
		if(!punishManager.cache.containsKey(sender.getUniqueId())) return true;
		for(UnknownCache cache : punishManager.cache.get(sender.getUniqueId()).getUnknownCacheList())
		{
			if(cache.getPunishCategory() != PunishCategory.MASS_OR_FALSE_REPORTS) continue;
			if(cache.getExpire().getTime() > System.currentTimeMillis())
			{
				unknownCache = cache;
				canSend = false;
				break;
			}
		}

		if(canSend)
		{
			plugin.getLilyMessenger().request(
					new JsonBuilder("type", PunishType.REPORT.toString())
							.append("reporter", sender.getName())
							.append("reporterUUID", sender.getUniqueId().toString())
							.append("reason", reason.toString())
							.append("victim", args[0]),
					MessageChannel.CUSTOM
			);
			sender.sendMessage(Colour.Yellow + "Report successful, Staff have been informed.");
		}
		else
		{
			sender.sendMessage(Colour.Red + "You are restricted from using Reports.");
			sender.sendMessage(Colour.White + "You can next use this feature in,");
			sender.sendMessage(punishManager.getCalculation(null, 0, 0, unknownCache.getExpire().getTime()));
		}
		return true;
	}
}
