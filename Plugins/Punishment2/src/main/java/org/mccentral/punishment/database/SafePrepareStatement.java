package org.mccentral.punishment.database;

import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.util.Callback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class SafePrepareStatement
{
	private Punishment plugin;
	private PreparedStatement preparedStatement;

	public SafePrepareStatement(Punishment plugin, String query)
	{
		this.plugin = plugin;

		try
		{
			preparedStatement = plugin.getMySQL().getConnection().prepareStatement(query);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public SafePrepareStatement set(int index, Object value)
	{
		try
		{
			preparedStatement.setObject(index, value);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return this;
	}

	public void execute(Callback<Boolean> success, boolean async)
	{
		if(async)
		{
			new BukkitRunnable()
			{
				public void run()
				{
					try
					{
						preparedStatement.execute();

						new BukkitRunnable()
						{
							public void run()
							{
								success.call(true);
								close(preparedStatement, null);
							}
						}.runTask(plugin);

					} catch (SQLException e)
					{
						e.printStackTrace();
						new BukkitRunnable()
						{
							public void run()
							{
								success.call(false);
								close(preparedStatement, null);
							}
						}.runTask(plugin);
					}
				}
			}.runTaskAsynchronously(plugin);
		}
		else
		{
			try
			{
				preparedStatement.execute();
				success.call(true);
				close(preparedStatement, null);

			} catch (SQLException e)
			{
				e.printStackTrace();
				success.call(false);
				close(preparedStatement, null);
			}
		}
	}

	public void executeQuery(Callback<ResultSet> result, boolean async)
	{
		if (async)
		{
			new BukkitRunnable()
			{
				public void run()
				{
					try
					{
						ResultSet resultSet = preparedStatement.executeQuery();

						new BukkitRunnable()
						{
							public void run()
							{
								result.call(resultSet);
								close(preparedStatement, resultSet);
							}
						}.runTask(plugin);

					} catch (SQLException e)
					{
						e.printStackTrace();
						new BukkitRunnable()
						{
							public void run()
							{
								result.call(null);
								close(preparedStatement, null);
							}
						}.runTask(plugin);
					}
				}
			}.runTaskAsynchronously(plugin);
		}
		else
		{
			try
			{
				ResultSet resultSet = preparedStatement.executeQuery();
				result.call(resultSet);
				close(preparedStatement, resultSet);;

			} catch (SQLException e)
			{
				e.printStackTrace();
				result.call(null);
				close(preparedStatement, null);
			}
		}
	}

	public void close(PreparedStatement statement, ResultSet result)
	{
		try
		{
			if (statement != null && !statement.isClosed()) statement.close();
			if (result != null && !result.isClosed()) result.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
