package org.mccentral.punishment.inventory.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.mccentral.punishment.Punishment;

import java.util.HashSet;

/**
 * Created by Teddeh on 15/05/2016.
 */
public abstract class TeddehMenu implements InventoryHolder
{
	protected Punishment plugin;

	private final Inventory inventory;
	private TeddehMenu parent;

	private final String title;
	private final int rows;
	private final boolean closeOnNullClick, resetCursor;

	private HashSet<MenuItem> items;

	public TeddehMenu(Punishment plugin, String title, int rows)
	{
		this(plugin, title, rows, false, null, false);
	}

	public TeddehMenu(Punishment plugin, String title, int rows, boolean closeOnNullClick, boolean resetCursor)
	{
		this(plugin, title, rows, closeOnNullClick, null, resetCursor);
	}

	public TeddehMenu(Punishment plugin, String title, int rows, TeddehMenu parent, boolean resetCursor)
	{
		this(plugin, title, rows, false, parent, resetCursor);
	}

	public TeddehMenu(Punishment plugin, String title, int rows, boolean closeOnNullClick, TeddehMenu parent, boolean resetCursor)
	{
		this.plugin = plugin;
		this.title = title;
		this.rows = rows;
		this.parent = parent;
		this.closeOnNullClick = closeOnNullClick;
		this.resetCursor = resetCursor;

		items = new HashSet<>();
		inventory = Bukkit.createInventory(this, (rows * 9), title);
	}

	public void setParent(TeddehMenu parent)
	{
		this.parent = parent;
	}

	public void addItem(MenuItem item)
	{
		items.add(item);
	}

	public void openInventory(Player player)
	{
		if (inventory == null) return;

		inventory.clear();
		for (MenuItem item : items)
		{
			inventory.setItem(item.getIndex(), item.getItemStack());
		}

		player.openInventory(inventory);
	}

	@Override
	public Inventory getInventory()
	{
		return inventory;
	}

	public String getTitle()
	{
		return title;
	}

	public int getRows()
	{
		return rows;
	}

	public boolean isCloseOnNullClick()
	{
		return closeOnNullClick;
	}

	public TeddehMenu getParent()
	{
		return parent;
	}

	public HashSet<MenuItem> getItems()
	{
		return items;
	}

	public boolean isResetCursor()
	{
		return resetCursor;
	}
}
