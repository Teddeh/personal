package org.mccentral.punishment.inventory.punish.moreOptionsMenu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.TeddehMenu;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.PM_Head;
import org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.*;
import org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish.MO_AllowReports;
import org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish.MO_Unban;
import org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish.MO_UnbanIP;
import org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish.MO_Unmute;
import org.mccentral.punishment.inventory.punish.punishMenu.PunishMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MoreOptionsMenu extends TeddehMenu
{
	public MoreOptionsMenu(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(plugin, "More Options", 3, true, new PunishMenu(plugin, punishManager, playerCache, key, reason), true);

		MenuItem[] items = new MenuItem[]
		{
			new PM_Head(key, reason),

			new MO_ClearInventory(punishManager, playerCache, key, reason),
			new MO_ClearPunishments(punishManager, playerCache, key, reason),
			new MO_InappropriateSkin(punishManager, playerCache, key, reason),
			new MO_JoinSpam(punishManager, playerCache, key, reason),
			new MO_RevokeReport(punishManager, playerCache, key, reason),
			new MO_RevokeReport(punishManager, playerCache, key, reason),
			new MO_SpamTPA(punishManager, playerCache, key, reason),
			new MO_StaffReview(punishManager, playerCache, key, reason),
			new MO_Guidelines(),

			new MO_AllowReports(punishManager, key),
			new MO_Unban(punishManager, key),
			new MO_UnbanIP(punishManager, key),
			new MO_Unmute(punishManager, key),

			new MenuItem(18, new ItemStackBuilder(Material.ARROW).setName(Colour.Red + "Go Back").build()) {
				@Override public void click(Player player, ClickType clickType)
				{
					player.closeInventory();
					new PunishMenu(plugin, punishManager, playerCache, key, reason).openInventory(player);
				}
			}
		};

		for(MenuItem menuItem : items) addItem(menuItem);
	}
}
