package org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MO_Guidelines extends MenuItem
{
	public MO_Guidelines()
	{
		super(26, new ItemStackBuilder(Material.BOOK).setName(Colour.White + "Punishment Guidelines").build());
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		player.sendMessage(Colour.RedB + "Punishment Guidelines");
		player.sendMessage(Colour.YellowB + " > " + Colour.Gold + "http://mccentral.org/guidelinesredirect");
		player.closeInventory();
	}
}
