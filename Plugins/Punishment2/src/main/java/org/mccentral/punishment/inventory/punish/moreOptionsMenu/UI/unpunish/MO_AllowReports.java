package org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishType;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MO_AllowReports extends MenuItem
{
	private PunishManager punishManager;
	private String key;

	public MO_AllowReports(PunishManager punishManager, String key)
	{
		super(24, new ItemStackBuilder(Material.INK_SACK, (byte) 1).setName(Colour.White + "Remove Report Restriction").build());

		this.punishManager = punishManager;
		this.key = key;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.ALLOW_REPORT)) return;

		punishManager.unPunish(player, key, PunishType.REVOKE_REPORT);
		player.closeInventory();
	}
}
