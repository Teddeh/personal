package org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI.unpunish;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishType;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MO_Unmute extends MenuItem
{
	private PunishManager punishManager;
	private String key;

	public MO_Unmute(PunishManager punishManager, String key)
	{
		super(23, new ItemStackBuilder(Material.INK_SACK, (byte) 1).setName(Colour.White + "Unmute").build());

		this.punishManager = punishManager;
		this.key = key;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.UN_MUTE)) return;

		punishManager.unPunish(player, key, PunishType.MUTE, PunishType.MUTE_PERM);
		player.closeInventory();
	}
}
