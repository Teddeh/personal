package org.mccentral.punishment.inventory.punish.punishMenu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.PunishCache;
import org.mccentral.punishment.cache.type.BanCache;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.WarnCache;
import org.mccentral.punishment.inventory.api.TeddehMenu;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.PM_Head;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.*;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.category.PM_ChatOffenceCatagory;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.category.PM_ClientModificationCategory;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.category.PM_GameplayCategory;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.severity.PM_ChatOffenceSeverity;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.severity.PM_ClientModificationSeverity;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.severity.PM_GameplayOffenceSeverity;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

import java.util.UUID;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishMenu extends TeddehMenu
{
	public PunishMenu(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(plugin, "Punishment Menu", 6, true, true);

		MenuItem[] items = new MenuItem[]
		{
			new PM_Head(key, reason),

			//Chat Offence.
			new PM_ChatOffenceCatagory(playerCache),
			new PM_ChatOffenceSeverity.PM_ChatOffenceSeverity1(punishManager, playerCache, key, reason),
			new PM_ChatOffenceSeverity.PM_ChatOffenceSeverity2(punishManager, playerCache, key, reason),
			new PM_ChatOffenceSeverity.PM_ChatOffenceSeverity3(punishManager, playerCache, key, reason),

			//Gameplay Offence.
			new PM_GameplayCategory(playerCache),
			new PM_GameplayOffenceSeverity.PM_GameplayOffenceSeverity1(punishManager, playerCache, key, reason),
			new PM_GameplayOffenceSeverity.PM_GameplayOffenceSeverity2(punishManager, playerCache, key, reason),
			new PM_GameplayOffenceSeverity.PM_GameplayOffenceSeverity3(punishManager, playerCache, key, reason),

			//Client Modification Offence.
			new PM_ClientModificationCategory(playerCache),
			new PM_ClientModificationSeverity.PM_ClientModificationSeverity1(punishManager, playerCache, key, reason),
			new PM_ClientModificationSeverity.PM_ClientModificationSeverity2(punishManager, playerCache, key, reason),
			new PM_ClientModificationSeverity.PM_ClientModificationSeverity3(punishManager, playerCache, key, reason),


			new PM_Warning(punishManager, playerCache, key, reason),
			new PM_Kick(punishManager, playerCache, key, reason),
			new PM_Other(plugin, punishManager, playerCache, key, reason),

			new PM_AllPunishments(plugin, punishManager, playerCache, key, reason),

			new PM_PermBan(punishManager, playerCache, key, reason),
			new PM_TempBan(plugin, punishManager, playerCache, key, reason)
		};

		for(MenuItem menuItem : items) addItem(menuItem);

		int x = 0;
		for(int i = 45; i < 53; i++) {
			x += 1;
			if(playerCache.getPunishmentsInOrder().size() < x) continue;
			PunishCache punishCache = playerCache.getPunishmentsInOrder().get(x-1);
			if(punishCache.getId() <= 0) continue;

			if(punishCache instanceof BanCache)
			{
				BanCache cache = (BanCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Severity: " + Colour.WhiteB + cache.getSeverity() + Colour.Reset + " (" + Colour.RedB + cache.getSubSeverity() + Colour.Reset + ")",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
						)
						.setAmount(x)
						.build()));
			}
			if(punishCache instanceof MuteCache)
			{
				MuteCache cache = (MuteCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Severity: " + Colour.WhiteB + cache.getSeverity() + Colour.Reset + " (" + Colour.RedB + cache.getSubSeverity() + Colour.Reset + ")",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
						)
						.setAmount(x)
						.build()));
			}
			if(punishCache instanceof WarnCache)
			{
				WarnCache cache = (WarnCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
								)
						.setAmount(x)
						.build()));
			}
		}
	}
}
