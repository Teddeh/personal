package org.mccentral.punishment.inventory.punish.punishMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.allPunishmentsMenu.AllPunishmentsMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 26/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_AllPunishments extends MenuItem
{
	private Punishment plugin;
	private PunishManager punishManager;
	private PlayerCache playerCache;
	private String key, reason;

	public PM_AllPunishments(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(53, new ItemStackBuilder(Material.BOOK).setName(Colour.White + "Show ALL Offences").build());

		this.plugin = plugin;
		this.punishManager = punishManager;
		this.playerCache = playerCache;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		player.closeInventory();
		new AllPunishmentsMenu(plugin, punishManager, playerCache, key, reason).openInventory(player);
	}
}
