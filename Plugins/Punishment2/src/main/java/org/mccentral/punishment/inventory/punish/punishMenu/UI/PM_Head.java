package org.mccentral.punishment.inventory.punish.punishMenu.UI;

import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.PlayerRecogniser;
import org.mccentral.punishment.util.SkullBuilder;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_Head extends MenuItem
{
	public PM_Head(String name, String reason)
	{
		super(4, new SkullBuilder().setName(Colour.White + PlayerRecogniser.getAsPlayer(name)).setOwner(PlayerRecogniser.getAsPlayer(name)).setLore(Colour.Gray + reason).build());
	}
}
