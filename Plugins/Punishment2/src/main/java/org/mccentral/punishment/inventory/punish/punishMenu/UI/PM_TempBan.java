package org.mccentral.punishment.inventory.punish.punishMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.timeSelector.TimeSelectorMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 28/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_TempBan extends MenuItem
{
	private Punishment plugin;
	private PunishManager punishManager;
	private PlayerCache playerCache;
	private String key, reason;

	public PM_TempBan(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(18, new ItemStackBuilder(Material.REDSTONE).setName(Colour.Red + "Temp Ban").build());

		this.plugin = plugin;
		this.punishManager = punishManager;
		this.playerCache = playerCache;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.TEMP_BAN)) return;

		new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, null).openInventory(player);
	}
}
