package org.mccentral.punishment.inventory.punish.punishMenu.UI.category;

import org.bukkit.Material;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_ClientModificationCategory extends MenuItem
{
	public PM_ClientModificationCategory(PlayerCache playerCache)
	{
		super(15, new ItemStackBuilder(Material.DIAMOND_SWORD).setName(Colour.WhiteB + "Client Modifications").setLore(Colour.GrayI + "Punishment Type: Ban", "", Colour.Yellow + "Total Hack punishments: " + Colour.Gold + playerCache.getTimesPunished(-1, PunishCategory.CLIENT_MODIFICATION)).build());
	}
}
