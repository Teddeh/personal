package org.mccentral.punishment.message;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum MessageChannel
{
	PUNISH("punish"),
	REPORT("report"),
	UN_PUNISH("unpunish"),
	CUSTOM("custom");

	private String channel;

	MessageChannel(String channel)
	{
		this.channel = channel;
	}

	public String getChannel()
	{
		return channel;
	}
}
