package org.mccentral.punishment.severity;

import org.mccentral.punishment.PunishType;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class SubSeverity
{
	private int offence;
	private long time;
	private PunishType type;

	public SubSeverity(int offence, long time, PunishType type)
	{
		this.offence = offence;
		this.time = time;
		this.type = type;
	}

	public int getOffence()
	{
		return offence;
	}

	public long getTime()
	{
		return time;
	}

	public PunishType getType()
	{
		return type;
	}
}
