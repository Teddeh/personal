package org.mccentral.punishment.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by Teddeh on 13/04/2016.
 */
public class ItemStackBuilder implements Cloneable
{
	private ItemStack itemStack;
	private ItemMeta itemMeta;

	public ItemStackBuilder(Material material)
	{
		itemStack = new ItemStack(material);
		itemMeta = itemStack.getItemMeta();
	}

	public ItemStackBuilder(Material material, byte data)
	{
		itemStack = new ItemStack(material, 1, data);
		itemMeta = itemStack.getItemMeta();
	}

	public ItemStackBuilder setName(String name)
	{
		itemMeta.setDisplayName(name);
		return this;
	}

	public ItemStackBuilder setLore(String... lore)
	{
		itemMeta.setLore(Arrays.asList(lore));
		return this;
	}

	public ItemStackBuilder setAmount(int amount)
	{
		itemStack.setAmount(amount);
		return this;
	}

	public ItemStack build()
	{
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

	@Override
	public Object clone()
	{
		try
		{
			return super.clone();
		} catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
