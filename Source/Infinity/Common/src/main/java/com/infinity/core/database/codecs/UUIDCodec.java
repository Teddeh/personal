package com.infinity.core.database.codecs;

import com.infinity.core.database.DatabaseEngine;
import com.infinity.core.reflection.SafeField;

import java.util.UUID;

public class UUIDCodec extends DBCodec<UUID> {

    public UUIDCodec(DatabaseEngine engine) {
        super(engine);
    }

    @Override
    public boolean canEncode(Class<?> type) {
        return UUID.class.isAssignableFrom(type);
    }

    @Override
    public boolean canDecode(Class<?> type) {
        return canEncode(type);
    }

    @Override
    public Object encode(Class<?> type, SafeField field, UUID value) {
        return value.toString();
    }

    @Override
    public Object decode(Class<?> type, SafeField field, Object object) {
        return UUID.fromString((String) object);
    }
}
