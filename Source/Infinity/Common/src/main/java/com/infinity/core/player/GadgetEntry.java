package com.infinity.core.player;

import com.infinity.core.database.Model;
import com.infinity.core.database.Storable;
import lombok.Getter;
import lombok.Setter;

@Model
public class GadgetEntry {

    @Storable
    @Getter
    private String gadgetName;

    @Storable
    @Getter
    @Setter
    private long data;

    public GadgetEntry() {
    }

    public GadgetEntry(String gadgetName, long data) {
        this.gadgetName = gadgetName;
        this.data = data;
    }

}
