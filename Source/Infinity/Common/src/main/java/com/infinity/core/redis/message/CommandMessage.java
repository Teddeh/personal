package com.infinity.core.redis.message;

import com.infinity.core.redis.Message;
import lombok.Getter;

public class CommandMessage implements Message {

    @Getter
    private String command = "";

    public CommandMessage() {}

    public CommandMessage(String cmd) {
        command = cmd;
    }
}
