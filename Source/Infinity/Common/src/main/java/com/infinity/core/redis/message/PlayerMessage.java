package com.infinity.core.redis.message;

import com.infinity.core.redis.Message;
import lombok.Getter;

public class PlayerMessage implements Message {
    @Getter
    private String player, message;

    public PlayerMessage() {}

    public PlayerMessage(String playerReceiver, String message) {
        this.player = playerReceiver;
        this.message = message;
    }
}
