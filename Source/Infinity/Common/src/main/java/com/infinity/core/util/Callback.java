package com.infinity.core.util;

public interface Callback<T> {
    void call(T t);
}
