package com.infinity.core;

import com.google.common.collect.Lists;
import com.infinity.core.command.CommandHandler;
import com.infinity.core.database.DatabaseEngine;
import com.infinity.core.event.Updater;
import com.infinity.core.packet.PacketModule;
import com.infinity.core.player.GlobalPlayer;
import com.infinity.core.player.PlayerHandler;
import com.infinity.core.redis.message.CommandMessage;
import com.infinity.core.redis.message.PlayerMessage;
import com.infinity.core.redis.message.PlayerOnlineMessage;
import com.infinity.core.util.CommonUtil;
import com.infinity.core.util.UtilWorld;
import lombok.Getter;
import com.infinity.core.inventory.MenuManager;
import com.infinity.core.player.MsgCommand;
import com.infinity.core.redis.JedisServer;
import com.infinity.core.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public abstract class Core extends JavaPlugin implements Listener {
    @Getter
    private List<Module> modules = Lists.newArrayList();

    @Getter
    private static JedisServer jedis;

    public final void onEnable() {
        new DatabaseEngine("127.0.0.1", 27017);


        jedis = new JedisServer(UtilServer.getServerName());

        JedisServer.registerListener(PlayerMessage.class, (sender, message) -> {
            Player receive = Bukkit.getPlayer(message.getPlayer());
            if(receive != null) receive.sendMessage(message.getMessage());
        });

        JedisServer.registerListener(CommandMessage.class, (sender, message) -> {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), message.getCommand());
        });

        jedis.registerListener(PlayerOnlineMessage.class, UtilServer.getHandler());

        UtilWorld.init();
        UtilServer.registerListener(UtilServer.getHandler());
        CommandHandler.register(new MsgCommand());
        UtilServer.registerListener(this);

        enableModules(new Updater(), new PlayerHandler(), new CommandHandler(), new MenuManager(), new PacketModule());
        enable();
    }

    protected void enable() {}

    protected void disable(){}

    public void onDisable() {
        disable();
        modules.forEach(Module::disable);

    }

    public void enableModules(Module... mdls) {
        for (Module m : mdls) {
            Bukkit.getPluginManager().registerEvents(m, this);
            m.enable();
        }
        modules.addAll(Arrays.asList(mdls));
    }

    public <T extends Module> T getModule(Class<T> clazz) {
        Optional<Module> found = modules.stream().filter(c -> c.getClass().equals(clazz)).findFirst();
        if (found.isPresent()) return clazz.cast(found.get());
        return null;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        GlobalPlayer globalPlayer = CommonUtil.getGlobalPlayer(e.getPlayer().getUniqueId());
        globalPlayer.setLastName(e.getPlayer().getName());
        globalPlayer.setLastServer(Bukkit.getServerName());

    }


}
