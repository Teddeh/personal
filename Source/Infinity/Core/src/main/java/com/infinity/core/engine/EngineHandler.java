package com.infinity.core.engine;

import com.infinity.core.Module;
import com.infinity.core.engine.event.GameInitialisedEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class EngineHandler extends Module {

    private Game game;

    public EngineHandler(Game game) {
        this.game = game;
    }

    @Override
    public void enable() {
        //
    }

    @Override
    public void disable() {
        //
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        game.addGamePlayer(event.getPlayer());
    }

    @EventHandler
    public void onInit(GameInitialisedEvent event) {
        Game game = event.getGame();

    }

    public Game getGame() {
        return game;
    }
}
