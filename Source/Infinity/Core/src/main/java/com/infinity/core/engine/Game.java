package com.infinity.core.engine;

import com.infinity.core.engine.team.Team;
import lombok.Getter;
import com.infinity.core.engine.player.GamePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Game implements IGame {

    @Getter private List<GamePlayer> gamePlayers;
    @Getter private List<Team> teams;

    @Getter private GameType gameType;

    private int min, max;
    private String[] description;

    public Game(GameType gameType, int[] slots, String[] description) {
        this.gameType = gameType;
        this.min = slots[0];
        this.max = slots[1];
        this.description = description;

        teams = new ArrayList<>();
        gamePlayers = new ArrayList<>();

        addTeam(new Team());
    }

    @Override
    public void start() {}

    @Override
    public void stop() {}

    public void addTeam(Team team) {
        teams.add(team);
    }

    public void addGamePlayer(Player player) {
        gamePlayers.add(new GamePlayer(player.getUniqueId()));
    }
}
