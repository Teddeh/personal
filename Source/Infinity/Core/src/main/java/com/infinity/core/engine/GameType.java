package com.infinity.core.engine;

import lombok.Getter;

public enum GameType {
    TEST("Test", 1);

    @Getter private String name;
    @Getter private int id;

    GameType(String name, int id) {
        this.name = name;
        this.id = id;
    }
}
