package com.infinity.core.engine;

public interface IGame {
    void start();
    void stop();
}
