package com.infinity.core.engine;

import com.infinity.core.engine.team.Team;

public class TeamGame extends Game {

    public TeamGame(GameType gameType, Team[] team) {
        super(gameType);

        super.getTeams().clear();
        for(Team t : team) addTeam(t);
    }
}
