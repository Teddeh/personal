package com.infinity.core.engine.countdown;

import com.infinity.core.engine.Game;
import com.infinity.core.util.UtilText;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

public class Countdown {

    private int start;
    private int tick;
    private CountdownDisplay[] countdownDisplays;
    private Map<Integer, String> modules;

    private BukkitTask task;
    private Game game;
    private JavaPlugin plugin;

    public Countdown(JavaPlugin plugin, Game game, int start, CountdownDisplay... countdownDisplays) {
        this.plugin = plugin;
        this.game = game;
        this.start = start;
        this.tick = start;
        this.countdownDisplays = countdownDisplays;
        this.modules = new HashMap<>();
    }

    public void addDisplay(int tick, String text) {
        modules.put(tick, text);
    }

    public void removeDisplay(int tick) {
        if(modules.containsKey(tick)) modules.remove(tick);
    }

    public void run() {
        task = new BukkitRunnable() {
            public void run() {
                if(modules.containsKey(tick)) {
                    String text = modules.get(tick).replace("{0}", tick+"");
                    for(CountdownDisplay cd : countdownDisplays) {
                        switch (cd) {
                            case CHAT:
                                game.getGamePlayers().forEach(gamePlayer -> gamePlayer.getPlayer().sendMessage(text));
                                break;

                            case BOSS_BAR:
                                //TODO
                                break;

                            case ACTION_BAR:
                                game.getGamePlayers().forEach(gamePlayer -> UtilText.sendActionBar(gamePlayer.getPlayer(), text));
                                break;

                            case TITLE:
                                //TODO
                                break;
                        }
                    }
                }

                if(tick <= 0) {
                    this.cancel();
                    return;
                }

                tick--;
            }
        }.runTaskTimer(plugin, 0, 20);
    }

    public void forceStop() {
        if(task == null) return;
        task.cancel();
        tick = start;
    }
}
