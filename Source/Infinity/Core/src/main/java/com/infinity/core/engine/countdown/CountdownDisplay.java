package com.infinity.core.engine.countdown;

public enum CountdownDisplay {
    CHAT, BOSS_BAR, ACTION_BAR, TITLE,
}
