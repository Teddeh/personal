package com.infinity.core.engine.countdown;

import com.infinity.core.engine.Game;
import org.bukkit.plugin.java.JavaPlugin;

public class GameCountdown extends Countdown {

    public GameCountdown(JavaPlugin plugin, Game game) {
        super(plugin, game, 30, CountdownDisplay.CHAT);

        addDisplay(30, "Game starting in {0} seconds.");
        addDisplay(15, "Game starting in {0} seconds.");
        addDisplay(10, "Game starting in {0} seconds.");
        addDisplay(5, "Game starting in {0} seconds.");
        addDisplay(4, "Game starting in {0} seconds.");
        addDisplay(3, "Game starting in {0} seconds.");
        addDisplay(2, "Game starting in {0} seconds.");
        addDisplay(1, "Game starting in {0} second.");
        addDisplay(0, "Game is starting!");
    }
}
