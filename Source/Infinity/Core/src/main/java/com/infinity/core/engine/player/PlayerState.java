package com.infinity.core.engine.player;

public enum PlayerState {
    WAITING, PLAYING, SPECTATING
}
