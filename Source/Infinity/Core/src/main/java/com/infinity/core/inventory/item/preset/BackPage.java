package com.infinity.core.inventory.item.preset;

import com.infinity.core.util.builder.ItemBuilder;
import com.infinity.core.inventory.item.ClickableItem;
import org.bukkit.Material;

public class BackPage extends ClickableItem {

    public BackPage(int index) {
        super(index, new ItemBuilder(Material.ARROW).setName("Back Page").build(), true);
    }
}
