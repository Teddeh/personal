package com.infinity.core.inventory.item.preset;

import com.infinity.core.util.builder.ItemBuilder;
import com.infinity.core.inventory.item.ClickableItem;
import org.bukkit.Material;

public class NextPage extends ClickableItem {

    public NextPage(int index) {
        super(index, new ItemBuilder(Material.ARROW).setName("Next Page").build(), true);
    }
}
