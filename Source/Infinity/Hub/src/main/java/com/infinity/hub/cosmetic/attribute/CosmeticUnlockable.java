package com.infinity.hub.cosmetic.attribute;

import com.infinity.core.player.Rank;

public interface CosmeticUnlockable {
    Rank getRequiredRank();
}
