package org.mccentral.build.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.build.GamemodeType;
import org.mccentral.core.Core;
import org.mccentral.core.builder.ItemStackBuilder;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.inventory.CentralMenu;
import org.mccentral.core.inventory.MenuItem;
import org.mccentral.core.util.*;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class MapsCommand extends CentralCommand
{
	public MapsCommand(Core core)
	{
		super(
				core,
				"maps",
				new String[]{"list"},
				Rank.BUILDER,
				"/maps <gamemode>",
				"test",
				"test",
				Sender.BOTH
		);

	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		if(!(commandSender instanceof Player))
		{
			if(args.length == 0)
			{
				ServerUtil.log(Level.WARNING, "Incorrect arguments, /maps <gamemode>");
				ServerUtil.log(Level.WARNING, "Use '/maps help' for support.");
				return;
			}

			else if(args.length >= 1)
			{
				if(args[0].equalsIgnoreCase("help"))
				{
					ServerUtil.log(Level.INFO, " ");
					ServerUtil.log(Level.INFO, "Listing all directories:");
					for(GamemodeType gamemodeType : GamemodeType.values())
						ServerUtil.log(Level.INFO, gamemodeType.toString() + ", " + gamemodeType.getName());
					ServerUtil.log(Level.INFO, " ");
					return;
				}

				GamemodeType gamemode = null;
				for(GamemodeType gamemodeType : GamemodeType.values())
				{
					if(!args[0].equalsIgnoreCase(gamemodeType.toString()) && !args[0].equalsIgnoreCase(gamemodeType.getName()))
						continue;
					gamemode = gamemodeType;
					break;
				}

				if(gamemode == null)
				{
					ServerUtil.log(Level.INFO, "The directory, " + args[0] + " count not be recognised.");
					return;
				}

				File[] files = new File(Bukkit.getWorldContainer() + "/../maps/" + gamemode.getName()).listFiles();
				ServerUtil.log(Level.INFO, "Listing all '" + gamemode.getName() + "' maps:");
				for(File f : files) ServerUtil.log(Level.INFO, f.getName());
				ServerUtil.log(Level.INFO, " ");
				return;
			}
		}

		else if(commandSender instanceof Player)
		{
			Player sender = (Player) commandSender;
			try
			{
				CentralMenu directoryMenu = new DirectoryMenu(core, new File(Bukkit.getWorldContainer() + "/../maps/"), 6);
				directoryMenu.openInventory(sender);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	class DirectoryMenu extends CentralMenu
	{

		public DirectoryMenu(Core core, File directory, int rows) throws IOException
		{
			super(core, "dir:/" + directory.getName(), rows);

			if(directory == null || !directory.exists()) return;
			File[] files = directory.listFiles();

			File back = new File(directory.getParentFile().getCanonicalPath());
			ItemStack backButton = new ItemStackBuilder(Material.ARROW).setName(Colour.Red + "/../").setLore(Colour.Gray + "Go back").build();
			addItem(new MenuItem(0, backButton)
			{
				@Override
				public void click(Player player, ClickType clickType)
				{
					ServerUtil.log(Level.INFO, back.getPath());
					if(back.getPath().contains("\\maps"))
					{
						try
						{
							CentralMenu backMenu = new DirectoryMenu(core, back, rows);
							backMenu.openInventory(player);
						} catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				}
			});

			int i = 1;
			for(File f : files)
			{
				if(i > 53) break; //We don't need pages as we wont have 53+ maps in the works at once.

				Material material = f.isDirectory() ? Material.CHEST : Material.PAPER;
				String stackName = material == Material.CHEST ? Colour.Aqua : Colour.Green;
				ItemStack stack = new ItemStackBuilder(material).setName(stackName + f.getName()).build();

					boolean leveldat = false, worlddata = false;
					File[] insideFile = f.listFiles();
					for (File fi : insideFile)
					{
						if (leveldat && worlddata) break;

						if (fi.getName().contains("worlddata.yml")) worlddata = true;
						if (fi.getName().contains("level.dat")) leveldat = true;
					}

					if (leveldat && worlddata)
					{
						Material worldMaterial = null;
						File data = new File(f.getAbsolutePath() + "/worlddata.yml");
						if (!data.exists()) return;
						YamlConfiguration configuration = YamlConfiguration.loadConfiguration(data);
						if (!configuration.contains("WorldType")) return;
						World.Environment environment = World.Environment.valueOf(configuration.getString("WorldType"));
						if (environment == null) environment = World.Environment.NORMAL;
						else if (environment == World.Environment.NORMAL) worldMaterial = Material.GRASS;
						else if (environment == World.Environment.NETHER) worldMaterial = Material.NETHERRACK;
						else if (environment == World.Environment.THE_END) worldMaterial = Material.ENDER_STONE;
						final World.Environment env = environment;
						ItemStack worldItem = new ItemStackBuilder(worldMaterial).setName(Colour.Yellow + f.getName()).setLore(Colour.Gray + "WorldType: " + environment.toString()).build();
						addItem(new MenuItem(i, worldItem)
						{
							@Override
							public void click(Player player, ClickType clickType)
							{
								if(Bukkit.getWorld(f.getName()) != null)
								{
									player.teleport(new Location(Bukkit.getWorld(f.getName()), 0, 55, 0, 0, 0));
									PlayerUtil.message(player, "Moving to " + f.getName());
									return;
								}
								boolean worldExists = false;
								File[] serverFiles = new File(Bukkit.getWorldContainer().getAbsolutePath()).listFiles();
								for(File fi : serverFiles)
								{
									if(!fi.getName().equalsIgnoreCase(f.getName()))
										continue;
									worldExists = true;
									break;
								}

								if(worldExists)
								{
									World object = WorldUtil.generateWorld(f.getName(), env, true);
									player.teleport(new Location(object, 0, 55, 0, 0, 0));
									PlayerUtil.message(player, "Moving to " + f.getName());
									return;
								}

								FileUtil.copyFiles(f, new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + f.getName()));
								World w = WorldUtil.generateWorld(f.getName(), env, true);
								player.teleport(new Location(w, 0, 55, 0, 0, 0));
								PlayerUtil.message(player, "Moving to " + f.getName());
							}
						});

						i += 1;
						continue;
					}


				addItem(new MenuItem(i, stack)
				{
					@Override
					public void click(Player player, ClickType clickType)
					{
						if(material == Material.CHEST)
						{
							try
							{
								if (f.getName().contains("templateWorld")) return;

								CentralMenu forwardMenu = new DirectoryMenu(core, f, rows);
								forwardMenu.openInventory(player);
							}
							catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					}
				});

				i += 1;
			}
		}
	}
}
