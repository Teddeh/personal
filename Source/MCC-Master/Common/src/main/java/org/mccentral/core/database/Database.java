package org.mccentral.core.database;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class Database
{
    private HikariDataSource connectionPool;

    public Database(String host, String port, String user, String pass, String db)
    {

        connectionPool = new HikariDataSource();
        connectionPool.setMaximumPoolSize(10); //Maximum of 3000 concurrent connections - DONT CHANGE
        connectionPool.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource"); //org.mariadb.jdbc.MySQLDataSource later

        connectionPool.addDataSourceProperty("serverName", host);
        connectionPool.addDataSourceProperty("port", port);
        connectionPool.addDataSourceProperty("databaseName", db);
        connectionPool.addDataSourceProperty("user", user);
        connectionPool.addDataSourceProperty("password", pass);


        connectionPool.setConnectionTimeout(3000);
        connectionPool.setValidationTimeout(1000);

    }

    public void disable() { connectionPool.close(); }

    public SafeStatement prepareStatement(String sql, Object... arguments)
    {
        Connection connection = null;
        SafeStatement statement = null;

        try
        {
            connection = connectionPool.getConnection();
            statement = new SafeStatement(connection.prepareStatement(sql), connection, arguments);
        } catch (SQLException e)
        {
            e.printStackTrace();
            try
            {
                if (connection != null) connection.close();
            } catch (SQLException e1)
            {
                e1.printStackTrace();
            }
        }

        return statement;
    }

}

