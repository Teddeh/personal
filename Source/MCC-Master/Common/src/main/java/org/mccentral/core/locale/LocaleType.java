package org.mccentral.core.locale;

import java.util.Locale;

/**
 * Created: 14/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum LocaleType
{
	af_ZA(new Locale( "af", "ZA" ), false, "Afrikaans"),
	ar_SA(new Locale( "ar", "SA" ), false, "Arabic"),
	as_ES(new Locale( "as", "ES" ), false, "Asturian"),
	bg_BG(new Locale( "bg", "BG" ), false, "Bulgarian"),
	ca_ES(new Locale( "ca", "ES" ), false, "Catalan"),
	cs_CZ(new Locale( "cs", "CZ" ), false, "Czech"),
	cy_GB(new Locale( "cy", "GB" ), false, "Welsh"),
	da_DK(new Locale( "da", "DK" ), false, "Danish"),
	de_DE(new Locale( "de", "DE" ), false, "German"),
	el_GR(new Locale( "el", "GR" ), false, "Greece"),
	en_AU(new Locale( "en", "AU" ), false, "Australian English"),
	en_CA(new Locale( "en", "CA" ), false, "Canadian English"),
	en_GB(new Locale( "en", "GB" ), false, "British English"),
	en_PI(new Locale( "en", "PI" ), false, "Pirate English"),
	en_US(new Locale( "en", "US" ), true, "American English"), //Enabled
	eo_UY(new Locale( "eo", "UY" ), false, "Esperanto"),
	es_AR(new Locale( "es", "AR" ), false, "Argentinian Spanish"),
	es_ES(new Locale( "es", "ES" ), false, "Spanish"),
	es_MX(new Locale( "es", "MX" ), false, "Mexican Spanish"),
	es_UY(new Locale( "es", "UY" ), false, "Uruguayan Spanish"),
	es_VE(new Locale( "es", "VE" ), false, "Venezuelan Spanish"),
	et_EE(new Locale( "et", "EE" ), false, "Estonian"),
	eu_ES(new Locale( "eu", "ES" ), false, "Basque"),
	fi_FI(new Locale( "fi", "FI" ), false, "Finnish"),
	fr_FR(new Locale( "fr", "FR" ), false, "French"),
	fr_CA(new Locale( "fr", "CA" ), false, "Canadian French"),
	ga_IE(new Locale( "ga", "IE" ), false, "Irish"),
	gl_ES(new Locale( "gl", "ES" ), false, "Galician"),
	he_IL(new Locale( "he", "IL" ), false, "Hebrew"),
	hi_IN(new Locale( "hi", "IN" ), false, "Hindi"),
	hr_HR(new Locale( "hr", "HR" ), false, "Croatian"),
	hu_HU(new Locale( "hu", "HU" ), false, "Hungarian"),
	hy_AM(new Locale( "hy", "AM" ), false, "Armenian"),
	id_ID(new Locale( "id", "ID" ), false, "Indonesian"),
	is_IS(new Locale( "is", "IS" ), false, "Icelandic"),
	it_IT(new Locale( "it", "IT" ), false, "Italian"),
	ja_JP(new Locale( "ja", "JP" ), false, "Japanese"),
	ka_GE(new Locale( "ka", "GE" ), false, "Georgian"),
	ko_KR(new Locale( "ko", "KR" ), false, "Korean"),
	kw_GB(new Locale( "kw", "GB" ), false, "Cornwall"),
	la_VA(new Locale( "la", "VA" ), false, "Latin"),
	lb_LU(new Locale( "lb", "LU" ), false, "Luxembourgish"),
	lt_LT(new Locale( "lt", "LT" ), false, "Lithuanian"),
	lv_LV(new Locale( "lv", "LV" ), false, "Latvian"),
	ms_MY(new Locale( "ms", "MY" ), false, "Malay"),
	mt_MT(new Locale( "mt", "MT" ), false, "Maltese"),
	nl_NL(new Locale( "nl", "NL" ), false, "Dutch"),
	no_NO(new Locale( "no", "NO" ), false, "Norwegian"),
	cc_CT(new Locale( "cc", "CT" ), false, "Occitan"),
	pl_PL(new Locale( "pl", "PL" ), false, "Polish"),
	pt_BR(new Locale( "pt", "BR" ), false, "Brazilian Portuguese"),
	pt_PT(new Locale( "pt", "PT" ), false, "Portuguese"),
	gya_AA(new Locale( "gya", "AA" ), false, "Quenya (High Elvish)"),
	ro_RO(new Locale( "ro", "RO" ), false, "Romanian"),
	ru_RU(new Locale( "ru", "RU" ), false, "Russian"),
	sk_SK(new Locale( "sk", "SK" ), false, "Slovak"),
	sl_SL(new Locale( "sl", "SL" ), false, "Slovenian"),
	sr_RS(new Locale( "sr", "RS" ), false, "Serbian"),
	sv_SE(new Locale( "sv", "SE" ), false, "Swedish"),
	th_TH(new Locale( "th", "TH" ), false, "Thai"),
	tlh_AA(new Locale( "tlh", "AA" ), false, "Klingon"),
	tr_TR(new Locale( "tr", "TR" ), false, "Turkish"),
	uk_UA(new Locale( "uk", "UA" ), false, "Ukrainian"),
	va_ES(new Locale( "va", "ES" ), false, "Valencian"),
	vi_VN(new Locale( "vi", "VN" ), false, "Vietnamese"),
	zh_CN(new Locale( "zh", "CN" ), false, "Chinese (Simplified)"),
	zh_TW(new Locale( "zh", "TW" ), false, "Chinese (Traditional)");

	private Locale locale;
	private boolean enabled;
	private String county;

	LocaleType(Locale locale, boolean enabled, String county) {
		this.locale = locale;
		this.enabled = enabled;
		this.county = county;
	}

	public Locale getLocale()
	{
		return locale;
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public String getCounty()
	{
		return county;
	}

	public String getCode()
	{
		return this.toString();
	}

	public int getId()
	{
		return this.ordinal();
	}
}
