package org.mccentral.core.message;

/**
 * Created by Teddeh on 9/04/2016.
 */
public abstract class Message {

    public abstract void onReceive(String sender);
}
