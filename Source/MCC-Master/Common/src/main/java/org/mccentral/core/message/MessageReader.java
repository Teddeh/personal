package org.mccentral.core.message;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class MessageReader extends JedisPubSub
{
    private String serverName;
    private Gson gson = new Gson();
    private JsonParser jsonParser = new JsonParser();
    private String messagePackage = "org.mccentral.core.message";

    public MessageReader(String name)
    {
        this.serverName = name;
    }

    @Override
    public void onMessage(String channel, String message)
    {
        if(!isValid(message)) return;

        try
        {
            JsonObject label = (JsonObject) jsonParser.parse(message);
            String messageName = label.get("name").getAsString();
            String sender = label.get("sender").getAsString();
            String recipient = label.get("recipient").getAsString();

            if (recipient.equalsIgnoreCase("all") || recipient.equalsIgnoreCase(serverName))
            {
                try
                {
                    Class<?> messageClass = Class.forName(messagePackage + "." + messageName);
                    Message msg = (Message) gson.fromJson(label.getAsJsonObject("content"), messageClass);
                    msg.onReceive(sender);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean isValid(String str)
    {
        try
        {
            jsonParser.parse(str);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
