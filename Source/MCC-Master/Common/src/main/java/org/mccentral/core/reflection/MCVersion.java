package org.mccentral.core.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class MCVersion
{
    public static final String NMS_ROOT;
    public static final String CB_ROOT;

    static
    {
        String version = "";
        StringBuilder builder = new StringBuilder();
        for (int a = 0; a < 10; a++)
        {
            for (int b = 0; b < 10; b++)
            {
                for (int c = 0; c < 10; c++)
                {
                    builder.setLength(0);
                    builder.append("v").append(a).append("_").append(b).append("_R").append(c);
                    version = builder.toString();
                    if (checkVersion(version))
                    {
                        a = b = c = 10;
                    }
                }
            }
        }

        NMS_ROOT = "net.minecraft.server." + version + ".";
        CB_ROOT = "org.bukkit.craftbukkit." + version + ".";
    }

    private static boolean checkVersion(String version)
    {
        try
        {
            Class.forName("net.minecraft.server." + version + ".World");
            return true;
        } catch (ClassNotFoundException e)
        {
            return false;
        }
    }

    public static SafeClass getNMSClass(String name)
    {
        return new SafeClass(getClass(NMS_ROOT + name));
    }

    public static SafeClass getCBClass(String name)
    {
        return new SafeClass(getClass(CB_ROOT + name));
    }

    public static SafeField getNMSField(String className, String fieldName)
    {
        return getField(NMS_ROOT + className, fieldName);
    }

    public static SafeField getCBField(String className, String fieldName)
    {
        return getField(CB_ROOT + className, fieldName);
    }

    public static SafeMethod getNMSMethod(String className, String methodName, Class<?>... argTypes)
    {
        return getMethod(NMS_ROOT + className, methodName, argTypes);
    }

    public static SafeMethod getCBMethod(String className, String methodName, Class<?>... argTypes)
    {
        return getMethod(CB_ROOT + className, methodName, argTypes);
    }

    public static SafeField getField(String className, String fieldName)
    {
        try
        {
            Class<?> clazz = getClass(className);
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return new SafeField(field);
        } catch (Exception e)
        {
            Logger.getLogger("Minecraft").log(Level.WARNING, "Plugin tried to access unknown field", e);
            return null;
        }
    }

    public static SafeMethod getMethod(String className, String methodName, Class<?>... argTypes)
    {
        try
        {
            Class<?> clazz = getClass(className);
            Method method = clazz.getDeclaredMethod(methodName, argTypes);
            method.setAccessible(true);
            return new SafeMethod(method);
        } catch (Exception e)
        {
            Logger.getLogger("Minecraft").log(Level.WARNING, "Plugin tried to access unknown method", e);
            return null;
        }
    }

    public static Class<?> getClass(String name)
    {
        try
        {
            name = name.endsWith("[]") ? "[L" + name.substring(0, name.length() - 2) + ";" : name;
            Class cs = Class.forName(name);
            return cs;
        } catch (Exception e)
        {
            Logger.getLogger("Minecraft").log(Level.WARNING, "Plugin tried to access unkown class", e);
            return null;
        }
    }
}
