package org.mccentral.core.reflection;


import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class SafeConstructor
{
    private Constructor<?> _constructor;

    public SafeConstructor(Constructor<?> constructor)
    {
        _constructor = constructor;
    }

    public Object newInstance(Object... params)
    {
        try
        {
            return _constructor.newInstance(params);
        } catch (Exception e)
        {
            Logger.getLogger("Minecraft").log(Level.WARNING, "Plugin tried to access unknown class " + e.getMessage());
            return null;
        }
    }
}
