package org.mccentral.core;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.command.CommandRemover;
import org.mccentral.core.command.ShutdownCommand;
import org.mccentral.core.command.TCommand;
import org.mccentral.core.component.Component;
import org.mccentral.core.component.player.ChatHandler;
import org.mccentral.core.component.player.Loader;
import org.mccentral.core.database.Database;
import org.mccentral.core.database.MCCPlayer;
import org.mccentral.core.inventory.MenuManager;
import org.mccentral.core.message.JedisServer;
import org.mccentral.core.server.ServerType;
import org.mccentral.core.util.Messenger;
import org.mccentral.core.util.packets.PacketManager;
import org.mccentral.core.util.updater.Updater;

import java.util.*;


/**
 * Created by Teddeh on 09/04/2016.
 */
public abstract class Core extends JavaPlugin
{
    private static Core instance;
    private List<Component> _componentList = Lists.newArrayList();
    private List<String> allPlayers = Lists.newArrayList();
    private HashMap<UUID, MCCPlayer> players = Maps.newHashMap();
    private Database repository;
    private JedisServer jedisServer;
    private String serverName = "";

    //Only use for Jedis messaging, Common has no main class which is why we need a static instance.
    public static Core getInstance()
    {
        return instance;
    }

    @Override
    public final void onEnable()
    {
        instance = this;

        /** Load server name for jedis **/
        saveDefaultConfig();
        serverName = getConfig().getString("server");

        /** Init data modules **/
        repository = new Database("localhost", "3306", "root", "root", "mcc");
        jedisServer = new JedisServer(serverName);

        /** Enable default components **/
        enableComponents(new MenuManager(this), new PacketManager(this), new Loader(this), new ChatHandler(this));

        /** Commands */
        new CommandRemover();
        new TCommand(this);
        new ShutdownCommand(this);

        /** Initialise all components + sub core **/
        enable();
        _componentList.forEach(component -> component.load());

        /** Updater */
        new Updater(this).run();

        /** Locale | Translator */
        new Messenger();
    }

    protected void enableComponents(Component... components)
    {
        Arrays.asList(components).forEach(c -> _componentList.add(c));
    }

    @Override
    public final void onDisable()
    {
        _componentList.forEach(component -> component.disable());
        jedisServer.disable();
    }


    public <T extends Component> T getComponent(Class<T> clazz)
    {
        Optional<Component> found = _componentList.stream().filter(c -> c.getClass().equals(clazz)).findFirst();
        if (found.isPresent()) return clazz.cast(found.get());
        return null;

    }


    public final MCCPlayer getPlayer(UUID uuid)
    {
        return players.get(uuid);
    }

    public final void addPlayer(MCCPlayer player)
    {
        players.put(player.getUUID(), player);
    }

    public final void removePlayer(UUID uuid)
    {
        players.remove(uuid);
    }

    public final Database getRepository()
    {
        return repository;
    }

    public abstract ServerType getServerType();

    public abstract void enable();

    public abstract void disable();

    public JedisServer getJedis()
    {
        return jedisServer;
    }

    public void setPlayerList(List<String> playerList)
    {
        this.allPlayers = playerList;
    }
}
