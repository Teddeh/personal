package org.mccentral.core.chat;

import org.mccentral.core.util.Colour;

/**
 * Created by Teddeh on 11/04/2016.
 */
public enum ChatType
{
	ERROR("Error", Colour.Red),
	PERMISSION("Permission", Colour.Red);

	private final String message, color;

	ChatType(String message, String color)
	{
		this.message = message;
		this.color = color;
	}

	public String getMessage()
	{
		return message;
	}

	public String getColor()
	{
		return color;
	}
}
