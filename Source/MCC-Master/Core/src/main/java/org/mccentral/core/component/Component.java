package org.mccentral.core.component;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.mccentral.core.Core;

/**
 * Created by Teddeh on 10/04/2016.
 */
public abstract class Component implements Listener
{
	protected Core plugin;

	public Component(Core plugin)
	{
		this.plugin = plugin;
	} //Constructor only used for getting variables



	public final void load()
	{
		Bukkit.getPluginManager().registerEvents(this, plugin);
		onEnable();
	}

	public final void disable()
	{
		onDisable();
		HandlerList.unregisterAll(this);
	}

	protected abstract void onEnable(); //Don't make these non abstract
	protected abstract void onDisable();
}
