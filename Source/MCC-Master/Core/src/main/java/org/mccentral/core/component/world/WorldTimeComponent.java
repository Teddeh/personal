package org.mccentral.core.component.world;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class WorldTimeComponent extends Component
{
	private boolean lockTime;
	private int time;

	public WorldTimeComponent(Core plugin, boolean lockTime, int time)
	{
		super(plugin);
		this.lockTime = lockTime;
		this.time = time;
	}

	public WorldTimeComponent(Core plugin, boolean lockTime, int time, World world)
	{
		super(plugin);
		this.lockTime = lockTime;
		this.time = time;

		if(lockTime) world.setGameRuleValue("doDaylightCycle", "false");
		world.setTime(time);
	}

	public void setTime(int time, World... worlds)
	{
		this.time = time;
		for(World world : worlds)
		{
			if(lockTime) world.setGameRuleValue("doDaylightCycle", "false");
			world.setTime(time);
		}
	}

	@Override
	protected void onEnable() {
		for(World world : Bukkit.getWorlds())
		{
			if(lockTime) world.setGameRuleValue("doDaylightCycle", "false");
			world.setTime(time);
		}
	}

	@Override
	protected void onDisable() {

	}
}
