package org.mccentral.core.cosmetic;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.util.ServerUtil;

import java.util.logging.Level;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class CosmeticManager implements Listener
{
	private final JavaPlugin plugin;

	public CosmeticManager(JavaPlugin plugin)
	{
		this.plugin = plugin;
		register();
	}

	public final void register()
	{
		if(plugin == null)
		{
			ServerUtil.log(Level.INFO, "Tried registering Cosmetic, Null plugin.");
			return;
		}

		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	public final void unregister()
	{
		HandlerList.unregisterAll(this);
	}

	public final JavaPlugin getPlugin()
	{
		return plugin;
	}
}
