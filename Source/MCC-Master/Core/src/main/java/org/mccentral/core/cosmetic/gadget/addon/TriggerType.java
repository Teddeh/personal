package org.mccentral.core.cosmetic.gadget.addon;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum TriggerType
{
	RIGHT_CLICK,
	LEFT_CLICK
}
