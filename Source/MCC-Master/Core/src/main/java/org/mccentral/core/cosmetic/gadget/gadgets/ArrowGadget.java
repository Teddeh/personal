package org.mccentral.core.cosmetic.gadget.gadgets;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.cosmetic.addon.CosmeticBuyable;
import org.mccentral.core.cosmetic.addon.CosmeticUnlockable;
import org.mccentral.core.cosmetic.addon.RarityType;
import org.mccentral.core.cosmetic.gadget.Gadget;
import org.mccentral.core.cosmetic.gadget.GadgetType;
import org.mccentral.core.cosmetic.gadget.IGadgetType;
import org.mccentral.core.cosmetic.gadget.addon.GadgetAmmo;
import org.mccentral.core.cosmetic.gadget.addon.GadgetCooldown;
import org.mccentral.core.cosmetic.gadget.addon.TriggerType;
import org.mccentral.core.cosmetic.gadget.event.GadgetTriggerEvent;
import org.mccentral.core.util.AnnotationUtil;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
@IGadgetType(type = GadgetType.ARROW_JOY_RIDE)
public class ArrowGadget extends Gadget implements CosmeticBuyable, CosmeticUnlockable, GadgetAmmo, GadgetCooldown
{
	public ArrowGadget(JavaPlugin plugin)
	{
		super(plugin, "Arrow Joy Ride", 1, RarityType.COMMON, TriggerType.RIGHT_CLICK);
	}

	@Override
	@EventHandler
	public void trigger(GadgetTriggerEvent event)
	{
		Player player = event.getPlayer();
		player.sendMessage("Triggered");

		Projectile projectile = player.launchProjectile(Arrow.class);
		projectile.setCustomName("gadget");
		projectile.setVelocity(projectile.getVelocity().multiply(1.5).normalize());
		projectile.setPassenger(player);
	}

	@EventHandler
	public void onProjectileLand(ProjectileHitEvent event)
	{
		if(event.getEntity().getCustomName() == null) return;
		if(event.getEntity().getCustomName().equals("gadget")) event.getEntity().remove();
	}

	@Override
	public GadgetType getGadgetType()
	{
		return GadgetType.valueOf(AnnotationUtil.getString(this.getClass(), 1));
	}

	@Override
	public int getPrice()
	{
		return 3000;
	}

	@Override
	public double getRarity()
	{
		return 1.0;
	}

	@Override
	public int getMaxAmmo()
	{
		return 999999;
	}

	@Override
	public int getAmmoPerUse()
	{
		return 1;
	}

	@Override
	public long getCooldown()
	{
		return 3000;
	}

	@Override
	public long getDonorCooldown()
	{
		return 2000;
	}
}
