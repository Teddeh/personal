package org.mccentral.core.message;

import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;
import org.mccentral.core.Core;

import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;

/**
 * Created by Teddeh on 16/04/2016.
 * Sent out from bungee every 3 seconds with a list of all the players
 */
public class MessagePlayerList extends Message
{
    private String players;

    public MessagePlayerList(String players) {
        this.players = players;
    }

    public MessagePlayerList(Collection<Player> players) {
        this.players = StringUtils.join(players, " ");
    }

    @Override
    public void onReceive(String sender)
    {
        System.out.println("Updating player list");
        Core.getInstance().setPlayerList(Arrays.asList(players.split(Pattern.quote(" "))));
    }
}
