package org.mccentral.core.util;

import org.bukkit.Material;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class AnnotationUtil
{
	public static boolean isInteger(Object object)
	{
		return (object instanceof Integer);
	}

	public static boolean isString(Object object)
	{
		return (object instanceof String);
	}

	public static boolean isBoolean(Object object)
	{
		return (object instanceof Boolean);
	}

	public static boolean isMaterial(Object object)
	{
		return (Material.valueOf(object.toString()) != null);
	}

	public static Collection<Material> getMaterials(Class<?> clazz)
	{
		List<Material> temp = new ArrayList<Material>();
		for(Object object : getAnnotationValues(clazz))
		{
			if(isInteger(object))
				continue;

			if(isString(object))
				continue;

			if(isMaterial(object))
				temp.add(Material.valueOf(object.toString()));
		}

		return (temp.isEmpty() ? null : temp);
	}

	public static String getString(Class<?> clazz, int index)
	{
		String str = "";
		for(Annotation annotation : clazz.getAnnotations())
		{
			Class<? extends Annotation> type = annotation.annotationType();
			int i = 1;
			for(Method method : type.getDeclaredMethods())
			{
				if(i != index)
				{
					i += 1;
					continue;
				}

				try
				{
					Object object = method.invoke(annotation, (Object[])null);
					str = object.toString();
				}
				catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}
		}

		return str;
	}

	public static Collection<Object> getAnnotationValues(Class<?> clazz)
	{
		List<Object> temp = new ArrayList<>();
		for(Annotation annotation : clazz.getAnnotations())
		{
			Class<? extends Annotation> type = annotation.annotationType();
			for(Method method : type.getDeclaredMethods())
			{
				try
				{
					Object object = method.invoke(annotation, (Object[])null);
					temp.add(object);
				}
				catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}
		}

		return (temp.isEmpty() ? null : temp);
	}
}
