package org.mccentral.core.util;

import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.mccentral.core.Core;
import org.mccentral.core.chat.ChatType;
import org.mccentral.core.reflection.MCVersion;
import org.mccentral.core.reflection.SafeClass;
import org.mccentral.core.reflection.SafeMethod;
import org.mccentral.core.util.packets.Packet;
import org.mccentral.core.util.packets.PacketManager;
import org.mccentral.core.util.packets.PacketType;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class PlayerUtil
{

	public static void message(Player player, String message)
	{
		player.sendMessage(message);
	}

	public static void message(Player player, String prefix, String message)
	{
		message(player, Colour.Yellow + prefix + Colour.Reset + " " + message);
	}

	public static void message(Player player, ChatType chatType, String message)
	{
		message(player, chatType.getColor() + chatType.getMessage() + Colour.Reset + " " + message);
	}

	public static Object getEntityPlayer(Player player)
	{
		((CraftPlayer) player).getHandle();

		SafeClass craftPlayer = MCVersion.getCBClass("CraftPlayer");
		Object object = craftPlayer.getHandle().cast(player); //This is ((CraftPlayer) player)

		SafeMethod getHandle = craftPlayer.getMethod("getHandle");

		return getHandle.invoke(object);
	}

	public static void sendTitle(Player player, String tit)
	{
		Packet title = Core.getInstance().getComponent(PacketManager.class).createPacket(PacketType.OUT_TITLE);
		title.set("b", new ChatMessage(tit)).set("a", PacketPlayOutTitle.EnumTitleAction.TITLE);
		Core.getInstance().getComponent(PacketManager.class).sendPacket(player, title);
	}

	public static void sendSubtitle(Player player, String sub)
	{
		//Subtitle (bottom)
		Packet subtitle = Core.getInstance().getComponent(PacketManager.class).createPacket(PacketType.OUT_TITLE);
		subtitle.set("b", new ChatMessage(sub)).set("a", PacketPlayOutTitle.EnumTitleAction.SUBTITLE);
		Core.getInstance().getComponent(PacketManager.class).sendPacket(player, subtitle);
	}

	public static void sendPacket(Player player, net.minecraft.server.v1_8_R3.Packet... packets)
	{
		PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
		for(net.minecraft.server.v1_8_R3.Packet packet : packets) connection.sendPacket(packet);
	}
}
