package org.mccentral.core.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.omg.CORBA.Object;

/**
 * Created: 24/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TabUtil
{
//	private final String[] alphabet = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

	public static void playerRank(Player player, String prefix, Object rank, Object[] ranks)
	{
		int x = ranks.length;
		for (int i = 0; i < ranks.length; i++)
		{
			if (!rank.equals(ranks[i])) continue;
			x = i;
		}

		Scoreboard board = player.getScoreboard();
		if (board == null)
		{
			board = Bukkit.getScoreboardManager().getMainScoreboard();
			player.setScoreboard(board);
		}

		Team team = null;
		for (Team t : board.getTeams())
		{
			if (!t.getName().equals("a" + x)) continue;
			team = t;
		}

		if (team == null)
		{
			team = board.registerNewTeam("a" + x);
			team.setPrefix(prefix);
		}

		team.addEntry(player.getName());

		for (Player all : Bukkit.getOnlinePlayers())
		{
			if (all.getScoreboard().equals(board)) continue;
			all.setScoreboard(board);
		}
	}

	public static void remove(Player player)
	{
		Scoreboard board = player.getScoreboard();

	}

	public static void cleanUp()
	{
		Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
		for(Team t : board.getTeams()) t.getEntries().clear();
		board.getTeams().clear();
	}
}
