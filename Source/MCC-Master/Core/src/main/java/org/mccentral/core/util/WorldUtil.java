package org.mccentral.core.util;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.util.logging.Level;

/**
 * Created by Teddeh on 13/04/2016.
 */
public class WorldUtil
{
	public static World generateWorld(String worldName, World.Environment environment, boolean autoSave)
	{
		World world = Bukkit.getWorld(worldName);
		if (world == null)
		{
			WorldCreator creator = new WorldCreator(worldName);
			creator.environment(environment);
			creator.generateStructures(false);
			world = creator.createWorld();
			world.setAutoSave(autoSave);
			world.setTime(0);
			ServerUtil.log(Level.INFO, worldName + " generated.");
			world.setGameRuleValue("doDaylightCycle", "false");
			return world;
		}
		return null;
	}

	public static void unloadWorld(String world, boolean save)
	{
		World w = Bukkit.getWorld(world);
		if (w == null) return;
		unloadWorld(w, save);
	}

	public static void unloadWorld(World world, boolean save)
	{
		Bukkit.unloadWorld(world, save);
	}
}
