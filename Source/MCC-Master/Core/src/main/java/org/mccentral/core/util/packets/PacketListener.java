package org.mccentral.core.util.packets;

/**
 * Created by Teddeh on 12/04/2016.
 */
public interface PacketListener {

    public void onPacketReceive(PacketEvent event);

    public void onPacketSend(PacketEvent event);
}
