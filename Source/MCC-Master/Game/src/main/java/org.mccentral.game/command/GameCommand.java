package org.mccentral.game.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.core.Core;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.util.Rank;
import org.mccentral.game.GameManager;
import org.mccentral.game.component.game.state.GameState;

/**
 * Created by Teddeh on 16/04/2016.
 */
public class GameCommand extends CentralCommand
{
	private GameManager gameManager;

	public GameCommand(Core core, GameManager gameManager)
	{
		super(
				core,//javaPlugin
				"game",//command name (/game)
				new String[]{},//aliases
				Rank.DEVELOPER,//rank
				"/game help",//description
				"test",//tooltip (useless for now)
				"test",//useless for now
				Sender.PLAYER,//sender type, PLAYER, CONSOLE, BOTH (its custom, something i thought could be cool)
				new Rank[]{}//blacklisted commands
		);

		this.gameManager = gameManager;
	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		Player sender = (Player) commandSender;
		if(args.length == 1)
		{
			if(args[0].equalsIgnoreCase("stop"))
			{
				gameManager.stop();
				return;
			}
		}
	}
}
