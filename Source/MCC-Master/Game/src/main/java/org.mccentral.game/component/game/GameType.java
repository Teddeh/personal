package org.mccentral.game.component.game;

/**
 * Created by Teddeh on 14/04/2016.
 */
public enum GameType
{
	SURVIVAL_GAMES("Survival Games"),
	SKYWARS("Skywars");

	private String name;

	GameType(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}
