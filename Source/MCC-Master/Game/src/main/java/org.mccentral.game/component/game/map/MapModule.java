package org.mccentral.game.component.game.map;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.mccentral.core.util.FileUtil;
import org.mccentral.core.util.ServerUtil;
import org.mccentral.core.util.WorldUtil;
import org.mccentral.game.component.game.GameType;

import java.io.File;
import java.util.Random;
import java.util.logging.Level;

/**
 * Created by Teddeh on 14/04/2016.
 */
public class MapModule
{
	private GameType game;

	public MapModule(GameType game)
	{
		this.game = game;
	}

	public String loadRandomMap()
	{
		File[] main = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/../maps/" + game.getName()).listFiles();
		final File random = main[new Random().nextInt(main.length)];

		ServerUtil.log(Level.INFO, "Random map chosen: " + random.getName());
		File to = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + random.getName());
		if(!to.exists()) to.mkdir();
		FileUtil.copyFiles(random, to);
		WorldUtil.generateWorld(random.getName(), World.Environment.NORMAL, false);

		return random.getName();
	}
}
