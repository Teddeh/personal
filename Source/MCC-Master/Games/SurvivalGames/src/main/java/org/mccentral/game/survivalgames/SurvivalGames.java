package org.mccentral.game.survivalgames;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.core.util.ServerUtil;
import org.mccentral.game.Game;
import org.mccentral.game.component.game.CentralGame;
import org.mccentral.game.component.game.GameType;
import org.mccentral.game.component.game.kit.CentralKit;
import org.mccentral.game.component.game.map.MapDataModule;
import org.mccentral.game.component.game.scoreboard.CentralScoreboard;
import org.mccentral.game.component.game.state.GameState;
import org.mccentral.game.survivalgames.companion.CompanionManager;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionEntityPart;
import org.mccentral.game.survivalgames.kits.TestKit;
import org.mccentral.game.survivalgames.kits.TestKit2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class SurvivalGames extends CentralGame
{
	private HashSet<Location> startChests;
	private HashSet<Location> chests;

	//TODO
	private CompanionManager companionManager;

	public SurvivalGames(Game game)
	{
		super(
				game,
				1,
				"Survival Games",
				GameType.SURVIVAL_GAMES,

				new String[]
				{
					"This is",
					"a test",
					"description"
				},

				new CentralKit[]
				{
					new TestKit(),
					new TestKit2()
				}
		);

		startChests = new HashSet<>();
		chests = new HashSet<>();

		companionManager = new CompanionManager(game);
		new aCommand(game, companionManager);
//		CompanionEntityPart.register();
	}

	@Override
	public void initialise()
	{
		super.initialise();

		ServerUtil.log(Level.INFO, "Survival Games Initialised!");

		MapDataModule dataModule = game.getGameManager().getMapDataModule();
		if(dataModule == null)
		{
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "shutdown");
			return;
		}

		dataModule.getSpawnLocations("BLACK").forEach(location -> dataModule.register(location, true, false));
		for(Location location : dataModule.getDataLocations("RED"))
		{
			BlockFace face = dataModule.getSignFace(location);
			if(face == null) continue;

			dataModule.register(location, true, true);
			location.getBlock().setType(Material.CHEST);
			startChests.add(location);
//			ServerUtil.log(Level.INFO, "Chest spawned facing: " + face.toString());
		}

		for(Location location : dataModule.getDataLocations("LIME"))
		{
//			BlockFace face = dataModule.getSignFace(location);
//			if(face == null) continue;

			dataModule.register(location, true, false);
			location.getBlock().setType(Material.CHEST);
			chests.add(location);
//			ServerUtil.log(Level.INFO, "Chest spawned facing: " + face.toString());
		}

		List<Location> testList = new ArrayList<>();
		for(Location location : dataModule.getDataLocations("AQUA"))
		{
			dataModule.register(location, true, false);
			for(int i = 0; i < 10; i++)
			{
				Location loc = new Location(location.getWorld(), location.getBlockX(), location.getBlockY()+i, location.getBlockZ());
				location.getWorld().getBlockAt(loc).setType(Material.SANDSTONE);
				if(i == 9) testList.add(loc);
			}
		}

		new BukkitRunnable()
		{
			int x = 0;
			public void run()
			{
				if(x == 10)
				{
					this.cancel();
					return;
				}

				testList.forEach(location -> {
					location.getWorld().getBlockAt(location.getBlockX(), location.getBlockY()-x, location.getBlockZ()).setType(Material.AIR);
				});

				x++;
			}
		}.runTaskTimer(game, 20*60L, 5L);
	}

	@Override
	public void scoreboard()
	{
		CentralScoreboard sgBoard = new CentralScoreboard("TITLE");
		for (int i = 1; i < 11; i++) sgBoard.setScore("Entry: " + i, i);
		addScoreboard(GameState.INGAME, sgBoard);
	}
}
