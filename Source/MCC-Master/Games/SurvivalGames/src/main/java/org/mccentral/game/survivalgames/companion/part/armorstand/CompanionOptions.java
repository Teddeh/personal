package org.mccentral.game.survivalgames.companion.part.armorstand;

import org.bukkit.entity.ArmorStand;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionOptions
{
	private final boolean small, gravity, visible;

	public CompanionOptions(boolean small, boolean gravity, boolean visible)
	{
		this.small = small;
		this.gravity = gravity;
		this.visible = visible;
	}

	public void apply(ArmorStand armorStand)
	{
		armorStand.setSmall(small);
		armorStand.setGravity(gravity);
		armorStand.setVisible(visible);
	}
}
