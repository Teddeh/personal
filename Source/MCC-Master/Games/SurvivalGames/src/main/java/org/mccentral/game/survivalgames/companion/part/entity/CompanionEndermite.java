package org.mccentral.game.survivalgames.companion.part.entity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.util.EulerAngle;
import org.mccentral.core.util.LocationUtil;
import org.mccentral.core.util.PlayerUtil;
import org.mccentral.game.survivalgames.companion.Companion;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created: 01/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionEndermite extends EntityEndermite
{
	private Companion companion;

	public CompanionEndermite(World world, Companion companion)
	{
		super(world);

		this.companion = companion;
		noclip = true;
	}

	@Override
	public void m()
	{
		if(!companion.isMoving())
		{
			Location location = LocationUtil.lookAt(companion.getHolder().getLocation(), companion.getPlayer().getLocation(), true, false);
			yaw = location.getYaw();
			pitch = location.getPitch();
			return;
		}

		super.m();
	}

	public CompanionEndermite spawn(Location loc)
	{
		net.minecraft.server.v1_8_R3.World w = ((CraftWorld) loc.getWorld()).getHandle();
		CompanionEndermite ent = new CompanionEndermite(w, companion);
		w.addEntity(ent, CreatureSpawnEvent.SpawnReason.CUSTOM);
		ent.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(ent);
		Bukkit.getOnlinePlayers().forEach(all -> PlayerUtil.sendPacket(all, packet));
		return ent;
	}

	public EulerAngle fromVector3f(Vector3f old) {
		return new EulerAngle(Math.toRadians((double)old.getX()), Math.toRadians((double)old.getY()), Math.toRadians((double)old.getZ()));
	}

	public Vector3f fromEulerAngle(EulerAngle old) {
		return new Vector3f((float)Math.toDegrees(old.getX()), (float)Math.toDegrees(old.getY()), (float)Math.toDegrees(old.getZ()));
	}
}
