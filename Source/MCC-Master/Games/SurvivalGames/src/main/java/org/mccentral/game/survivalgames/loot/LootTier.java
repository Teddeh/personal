package org.mccentral.game.survivalgames.loot;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum LootTier
{
	TIER_1,
	TIER_2,
	TIER_3,
	SPECIAL
}
