package org.mccentral.game.survivalgames.loot.item;

import org.bukkit.inventory.ItemStack;
import org.mccentral.game.survivalgames.loot.LootTier;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class SpecialItem extends LootItem
{
	public SpecialItem(ItemStack itemStack)
	{
		super(itemStack, LootTier.SPECIAL);
	}
}
