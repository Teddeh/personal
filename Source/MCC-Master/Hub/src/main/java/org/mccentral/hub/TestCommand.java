package org.mccentral.hub;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.core.Core;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.cosmetic.gadget.gui.GadgetMenu;
import org.mccentral.core.util.Rank;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TestCommand extends CentralCommand
{
	private Hub hub;

	public TestCommand(Hub hub)
	{
		super(
				hub,
				"test",
				new String[]{},
				Rank.MEMBER,
				"",
				"",
				"",
				Sender.PLAYER,
				new Rank[]{}
		);

		this.hub = hub;
	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		new GadgetMenu(hub, hub.getGadgetManager()).openInventory((Player) commandSender);
	}
}
