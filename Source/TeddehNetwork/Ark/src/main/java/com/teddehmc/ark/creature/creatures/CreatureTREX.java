package com.teddehmc.ark.creature.creatures;

import com.teddehmc.ark.attribute.DamageStructureAttribute;
import com.teddehmc.ark.creature.*;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CreatureTREX extends Creature implements HostileCreature, LandCreature, TamableCreature, RideableCreature, DamageStructureAttribute
{
	public CreatureTREX(int level)
	{
		super(level, CreatureType.TREX);
	}

	@Override
	public CreatureDisturbType getDisturbType()
	{
		return CreatureDisturbType.IN_RANGE;
	}

	@Override
	public double getDamage()
	{
		return 0;//TODO
	}
}
