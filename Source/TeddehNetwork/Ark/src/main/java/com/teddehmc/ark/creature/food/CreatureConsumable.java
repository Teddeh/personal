package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.creature.Creature;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface CreatureConsumable
{
	void onCreatureConsume(Creature creature);
}
