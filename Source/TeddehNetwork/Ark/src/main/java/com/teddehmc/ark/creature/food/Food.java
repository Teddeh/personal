package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.misc.ArkItem;
import com.teddehmc.ark.material.ArkMaterial;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Food extends ArkItem implements IFood
{
	private double healthRegen, foodRegen, saturation;

	public Food(int id, String name, double weight, double foodRegen, double healthRegen, double saturation)
	{
		super(id, name, weight);

		this.foodRegen = foodRegen;
		this.healthRegen = healthRegen;
		this.saturation = saturation;
	}

	@Override
	public double getFoodRegeneration()
	{
		return foodRegen;
	}

	@Override
	public double getHealthRegeneration()
	{
		return healthRegen;
	}

	@Override
	public double getSaturation()
	{
		return saturation;
	}
}
