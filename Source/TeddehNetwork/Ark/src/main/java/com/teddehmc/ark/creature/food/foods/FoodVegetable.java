package com.teddehmc.ark.creature.food.foods;

import com.teddehmc.ark.creature.food.Food;
import com.teddehmc.ark.creature.food.PlayerConsumable;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.user.ArkUser;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class FoodVegetable extends Food implements PlayerConsumable
{
	public FoodVegetable(int id, String name, double foodRegen, double healthRegen, double saturation)
	{
		super(
				id,
				name,
				0.25,
				foodRegen,
				healthRegen,
				saturation
		);
	}

	@Override
	public void onPlayerConsume(ArkUser user)
	{

	}
}
