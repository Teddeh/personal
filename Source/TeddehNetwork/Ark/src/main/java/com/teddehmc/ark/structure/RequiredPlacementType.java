package com.teddehmc.ark.structure;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum RequiredPlacementType
{
	ABOVE,
	AGAINST
}
