package com.teddehmc.ark.structure;

import com.teddehmc.ark.structure.data.AttachableStructure;
import com.teddehmc.ark.structure.data.RequiredStructure;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface StructureDimension
{
	int[][][][] getDimension();
	RequiredStructure[] getRequiredStructures();
	AttachableStructure[] getAttachableStructures();
}
