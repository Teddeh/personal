package com.teddehmc.ark.structure;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum StructureMaterial
{
	THATCH,
	WOOD,
	STONE,
	METAL,
	OTHER
}
