package com.teddehmc.ark.structure.data;

import com.teddehmc.ark.material.ArkMaterial;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class AttachableStructure
{
	private ArkMaterial arkMaterial;

	public AttachableStructure(ArkMaterial arkMaterial)
	{
		this.arkMaterial = arkMaterial;
	}

	public ArkMaterial getArkMaterial()
	{
		return arkMaterial;
	}
}
