package com.teddehmc.ark.structure.data;

import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.structure.Structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 09/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Building
{
	private ArkMaterial structure;
	private List<StructureBlock> structureBlocks;
	private boolean cancel = false, obstructed = false;

	public Building(ArkMaterial structure)
	{
		this.structure = structure;
		structureBlocks = new ArrayList<>();
	}

	public List<StructureBlock> getStructureBlocks()
	{
		return structureBlocks;
	}

	public void clearStructureBlocks(boolean restore)
	{
		if(restore)
		{
			structureBlocks.forEach(strBlock -> {
				strBlock.getLocation().getBlock().setType(strBlock.getOriginalMaterial());
				strBlock.getLocation().getBlock().setData(strBlock.getOriginalData());
			});
		}

		structureBlocks.clear();
	}

	public void refill(List<StructureBlock> strBlocks)
	{
		structureBlocks.clear();
		structureBlocks.addAll(strBlocks);
	}

	public ArkMaterial getStructureMaterial()
	{
		return structure;
	}

	public boolean isCancel()
	{
		return cancel;
	}

	public boolean isObstructed()
	{
		return obstructed;
	}

	public void setCancel(boolean cancel)
	{
		this.cancel = cancel;
	}

	public void setObstructed(boolean obstructed)
	{
		this.obstructed = obstructed;
	}
}
