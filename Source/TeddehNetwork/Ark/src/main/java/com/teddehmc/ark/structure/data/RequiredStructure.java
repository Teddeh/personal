package com.teddehmc.ark.structure.data;

import com.teddehmc.ark.material.ArkMaterial;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class RequiredStructure
{
	private ArkMaterial arkMaterial;
	private boolean fullContact;

	public RequiredStructure(ArkMaterial arkMaterial, boolean fullContact)
	{
		this.arkMaterial = arkMaterial;
		this.fullContact = fullContact;
	}

	public ArkMaterial getArkMaterial()
	{
		return arkMaterial;
	}

	public boolean isFullContact()
	{
		return fullContact;
	}
}
