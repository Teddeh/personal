package com.teddehmc.ark.structure.data;

import org.bukkit.Location;
import org.bukkit.Material;

/**
 * Created: 09/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureBlock
{
	private Location location;
	private Material material;
	private byte data;
	private boolean collide;

	public StructureBlock(Location location, Material material, byte data, boolean collide)
	{
		this.location = location;
		this.material = material;
		this.data = data;
		this.collide = collide;
	}

	public Location getLocation()
	{
		return location;
	}

	public Material getOriginalMaterial()
	{
		return material;
	}

	public byte getOriginalData()
	{
		return data;
	}

	public boolean canCollide()
	{
		return collide;
	}
}
