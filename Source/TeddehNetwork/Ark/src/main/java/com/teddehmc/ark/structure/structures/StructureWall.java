package com.teddehmc.ark.structure.structures;

import com.teddehmc.ark.crafting.CraftingRecipe;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.structure.ArkStructure;
import com.teddehmc.ark.structure.StructureMaterial;
import com.teddehmc.ark.structure.damage.StructureDamage;
import com.teddehmc.ark.structure.data.AttachableStructure;
import com.teddehmc.ark.structure.data.RequiredStructure;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureWall extends ArkStructure
{
	public StructureWall(int id, String name, int durability, double weight, StructureMaterial structureMaterial, StructureDamage[] structureDamage, CraftingRecipe craftingRecipe)
	{
		super(id, name, durability, weight, structureMaterial,
				new int[][][][] {
						{{{2,1,2},{0,0,0},{0,0,0}},{{2,1,2},{0,0,0},{0,0,0}},{{2,1,2},{0,0,0},{0,0,0}}},
						{{{0,0,2},{0,0,1},{0,0,2}},{{0,0,2},{0,0,1},{0,0,2}},{{0,0,2},{0,0,1},{0,0,2}}}
				},
				structureDamage, craftingRecipe);
	}

	@Override
	public RequiredStructure[] getRequiredStructures()
	{
		return new RequiredStructure[]
			{
				new RequiredStructure(ArkMaterial.STRUCTURE_FOUNDATION_THATCH, true)
			};
	}

	@Override
	public AttachableStructure[] getAttachableStructures()
	{
		return new AttachableStructure[]
			{
				new AttachableStructure(ArkMaterial.STRUCTURE_WALL_THATCH)
			};
	}
}
