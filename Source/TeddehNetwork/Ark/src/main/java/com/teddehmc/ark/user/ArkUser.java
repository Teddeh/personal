package com.teddehmc.ark.user;

import com.teddehmc.ark.misc.Controller;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ArkUser extends Controller
{
	private final UUID uuid;
	private final String name;

	public ArkUser(UUID uuid, String name)
	{
		this.uuid = uuid;
		this.name = name;
	}

	public ArkUser(Player player)
	{
		this(player.getUniqueId(), player.getName());
	}

	public Player getPlayer()
	{
		return Bukkit.getPlayer(uuid);
	}
}
