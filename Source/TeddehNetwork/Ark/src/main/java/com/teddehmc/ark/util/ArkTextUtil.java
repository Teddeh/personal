package com.teddehmc.ark.util;

import com.teddehmc.core.util.Color;

/**
 * Created: 10/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ArkTextUtil
{
	/**
	 * @param symbol   Symbol used to create the text
	 * @param amount   Amount of symbols
	 * @param color    If true, this will return GREEN else RED
	 * @return
	 */
	public static String getObstructedText(String symbol, int amount, boolean color)
	{
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < amount; i++)
		{
			String c = color ? Color.DarkRed : Color.DarkGreen;
			if ((i % 2) == 0) c = color ? Color.Red : Color.Green;
			builder.append(c + symbol);
		}

		return builder.toString();
	}
}
