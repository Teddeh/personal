package com.teddehmc.ark.weapon;

import com.teddehmc.ark.misc.ArkItem;
import com.teddehmc.ark.attribute.*;
import com.teddehmc.ark.crafting.CraftingRecipe;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Weapon extends ArkItem implements DurabilityAttribute, RepairAttribute, CraftableAttribute
{
	private int durability, maxDurability, damage;
	private CraftingRecipe craftingRecipe;

	public Weapon(int id, String name, int durability, double weight, int damage, CraftingRecipe craftingRecipe)
	{
		super(id, name, weight);

		this.durability = durability;
		this.maxDurability = durability;
		this.damage = damage;
		this.craftingRecipe = craftingRecipe;
	}

	@Override
	public int getItemDurability()
	{
		return durability;
	}

	@Override
	public int getMaxDurability()
	{
		return maxDurability;
	}

	/**
	 * This is the damage value caused upon combat, Player vs Player/Creature
	 * @return
	 */
	public int getDamage()
	{
		return damage;
	}

	@Override
	public CraftingRecipe getCraftingRecipe()
	{
		return craftingRecipe;
	}
}
