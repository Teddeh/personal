package com.teddehmc.ark.weapon.weapons;

import com.teddehmc.ark.crafting.CraftingRecipe;
import com.teddehmc.ark.weapon.Weapon;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class WeaponFist extends Weapon
{
	public WeaponFist()
	{
		super(
				1,         //ID
				"Fist",    //Name
				-1,        //Durability
				0,         //Weight
				5,         //Damage
				null       //Crafting Recipe
		);
	}
}
