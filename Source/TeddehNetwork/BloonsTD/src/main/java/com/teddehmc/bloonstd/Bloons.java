package com.teddehmc.bloonstd;

import com.teddehmc.core.IModule;
import com.teddehmc.core.ModuleState;
import com.teddehmc.core.Teddeh;

@IModule(name = "Bloons", moduleState = ModuleState.DEVELOPMENT)
public class Bloons extends Teddeh
{

	@Override
	public void load()
	{

	}

	@Override
	public void disable()
	{

	}
}
