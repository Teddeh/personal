package com.teddehmc.bloonstd.bloons;

public enum BalloonSkin
{
	RED("Red"),
	BLUE("Blue"),
	GREEN("Green"),
	YELLOW("Yellow"),
	PINK("Pink"),

	BLACK("Black"),
	WHITE("White"),

	LEAD("Lead"),

	ZEBRA("Zebra"),
	RAINBOW("Rainbow"),
	CERAMIC("Ceramic"),

	BLIMP("Blimp"),
	MOAB("MOAB"),
	ZOMG("Z.O.M.G");

	private String name;

	BalloonSkin(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public boolean isBoss()
	{
		return (this == BLIMP || this == MOAB || this == ZOMG);
	}
}
