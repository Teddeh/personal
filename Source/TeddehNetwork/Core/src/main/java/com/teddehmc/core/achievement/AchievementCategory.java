package com.teddehmc.core.achievement;

import com.teddehmc.core.util.Color;
import org.bukkit.ChatColor;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum AchievementCategory
{
	RECRUIT(Color.Yellow),
	REGULAR(Color.Green),
	HARDENED(Color.Blue),
	VETERAN(Color.Red);

	private String color;

	AchievementCategory(String color)
	{
		this.color = color;
	}

	public String getColor()
	{
		return color;
	}
}
