package com.teddehmc.core.achievement;

import com.teddehmc.core.util.TProgress;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface AchievementProgress<K, V>
{
	TProgress<K, V>[] getProgressArray();
}
