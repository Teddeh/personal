package com.teddehmc.core.component;

import org.bukkit.event.Listener;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface IComponent extends Listener
{
	void load();
	void disable();
}
