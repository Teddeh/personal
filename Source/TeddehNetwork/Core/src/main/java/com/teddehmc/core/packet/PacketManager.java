package com.teddehmc.core.packet;

import com.teddehmc.core.component.Component;
import com.teddehmc.core.component.IComponent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PacketManager extends Component
{
	private PacketInterceptor packetInterceptor;

	public PacketManager(JavaPlugin plugin, PacketInterceptor packetInterceptor)
	{
		super(plugin);

		this.packetInterceptor = packetInterceptor;
	}

	@Override
	public void load()
	{
		Bukkit.getOnlinePlayers().forEach(player -> packetInterceptor.create(player));
	}

	@Override
	public void disable()
	{
		Bukkit.getOnlinePlayers().forEach(player -> packetInterceptor.remove(player));
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		packetInterceptor.create(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		packetInterceptor.remove(event.getPlayer());
	}
}
