package com.teddehmc.core.reward;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TReward<T>
{
	public TReward(RewardType rewardType, T value)
	{
		//TODO
	}

	/**
	 * TODO: Complete custom reward.
	 *
	 * This method should only be overridden if the
	 * Reward type is not listed in RewardType.java
	 */
	public void customReward() {}
}
