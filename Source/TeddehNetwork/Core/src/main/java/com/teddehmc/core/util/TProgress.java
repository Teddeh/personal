package com.teddehmc.core.util;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TProgress<K, V>
{
	private K target;
	private V value;

	public TProgress(K target, V value)
	{
		this.target = target;
		this.value = value;
	}

	public K getTarget()
	{
		return target;
	}

	public V getValue()
	{
		return value;
	}

	public void setValue(V value)
	{
		this.value = value;
	}

	public boolean isComplete()
	{
		return value.equals(target);
	}
}
